﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuelBarScript : MonoBehaviour
{
    public WeaponScriptableObject lighter;
    private float MaxFuel;
    private float ActualFuel;
    // Start is called before the first frame update
    void Start()
    {
        MaxFuel = 100;
    }

   
    public void UpdateFuel()
    {
        MaxFuel = 100;
        ActualFuel = lighter.gas;

        this.GetComponent<Image>().fillAmount = ActualFuel / MaxFuel;
    }

}
