﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onColliderEnterToInv : MonoBehaviour
{
    private InventoryItem inventario;

    private void Awake()
    {
        this.inventario = this.transform.parent.root.GetComponentInChildren<InventoryController>().getInventoryItem();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "item")
        {
            var item = other.transform.gameObject.GetComponent<itemComponent>();
            if (item)
            {
                //print(item.getThisItem());
                if (inventario.Slot.Count != 24)
                {

                    inventario.AddItem(new ItemObject(item.getThisItem()), 1);
                    GameObject.Destroy(other.transform.gameObject);
                }
            }
            this.GetComponent<CapsuleCollider>().enabled = false;
        }
    }
}
