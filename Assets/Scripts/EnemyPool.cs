﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyAmountOnPool
{
    public string enemyGameObjectName;
    public GameObject enemyGameObject;
    public int amountOfEnemies;
}

public class EnemyPool : MonoBehaviour
{
    public static EnemyPool instance;
    public List<GameObject> enemiesPool;
    public List<EnemyAmountOnPool> enemiesForPool;
    private GameObject EnemysPoolGameObject;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance ==null) 
        {
            instance = this;
        }
        EnemysPoolGameObject = GameObject.Find("EnemysPoolGameObject");
        LoadPool();
    }

    private void LoadPool()
    {
        foreach (EnemyAmountOnPool enemy in enemiesForPool)
        {
            for (int i = 0; i < enemy.amountOfEnemies; i++)
            {
                GameObject tmp;
                tmp = Instantiate(enemy.enemyGameObject, EnemysPoolGameObject.transform.position, Quaternion.identity);
                tmp.gameObject.transform.SetParent(EnemysPoolGameObject.transform);
                tmp.gameObject.name = enemy.enemyGameObjectName;
                tmp.SetActive(false);
                enemiesPool.Add(tmp);
            }
        }
    }

    public GameObject getEnemy(string enemy)
    {
        for (int i = 0; i < enemiesPool.Count; i++)
        {
            if(enemiesPool[i].gameObject.name == enemy)
            {
                for (int j = 0; j < EnemysPoolGameObject.transform.childCount; j++)
                {
                    if (!enemiesPool[j].activeInHierarchy && enemiesPool[j].gameObject.name == enemy)
                    {
                        return enemiesPool[j];
                    }
                }
            }
        }
        return null;
    }

    public void returnEnemy(GameObject enemy)
    {
        if (enemiesPool.Contains(enemy))
        {
            enemy.transform.gameObject.GetComponent<IAStats>().imDead = false;
            enemy.transform.gameObject.GetComponent<IAStats>().LoadStats();
            enemy.transform.position = EnemysPoolGameObject.transform.position;
            enemy.gameObject.transform.SetParent(EnemysPoolGameObject.transform);
            enemy.SetActive(false);
        }
    }
}
