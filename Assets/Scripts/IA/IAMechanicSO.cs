﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class IAMechanicSO : ScriptableObject
{
    public float area;
    public float cdPercent;
    public float cd;
    public float health;
    public float armor;
    public float damage;
}
