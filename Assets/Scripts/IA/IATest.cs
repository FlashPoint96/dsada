﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class IATest : MonoBehaviour
{
    public GameObject player;
    public GameObject target;
    public GameObject aux;

    [SerializeField]
    private Vector3[] points = new Vector3[5];

    public GameObject meetDrop;

    private NavMeshAgent navMess;
    private int rand;
    private Vector3 startPosition;
    private Animator anim;
    private SphereCollider damageArea;

    private float animDuration;

    [Header("STATS")]
    public float area;
    public float cdPercent;
    public float cd;
    public float nextAttackTime;
    public float health;
    public float armor;
    public float damage;
    [SerializeField]
    private float distance;
    private Vector3 radius;
    
    [Header("STATES")]
    public bool patrol;
    public bool inPatrol;
    public bool chase;
    public bool inChase;
    public bool inAttack;
    public bool imDead;


    [Header("HITS")]
    public bool hitPlayer;
    public bool hitBoar;
    

    void Start()
    {
        player = FindObjectOfType<FPSPlayerWeaponController>().gameObject;
        Load();
        Patrol();
        //navMess.SetDestination(target.position);
    }

    void Update()
    {
        //LookAtMe();
        //Chase();
        //Idle();
        StateMachine();

    }

    private void StateMachine()
    {
        if (patrol)
        {
            if (!inPatrol)
            {
                Patrol();
            }
        }
        if(chase)
        {
            patrol = false;
            inPatrol = false;

            if (!inChase)
            {
                Chase(target);
            }
        }
    }

    private void Patrol()
    {
        inPatrol = true;
        StartCoroutine(Goto());
    }

    IEnumerator Goto()
    {
        if (inPatrol)
        {
            anim.SetBool("walk", true);
            navMess.SetDestination(RandomNavmeshLocation(50f));
            //navMess.destination = target;
            rand = Random.Range(6, 9);
            yield return new WaitForSeconds(rand);
            StartCoroutine(Idle());
        }
    }

    IEnumerator Idle()
    {
        if(inPatrol)
        {
            anim.SetBool("walk", false);
            //navMess.SetDestination(this.transform.position);
            rand = Random.Range(4, 9);
            yield return new WaitForSeconds(rand);
            StartCoroutine(Goto());
        }
    }

   /* private Vector3 GetTarget()
    {
        //float x = Random.Range(-1f, 1f) * area;
        //float z = Random.Range(-1f, 1f) * area;
        //return startPosition + new Vector3(x, 0, z);
        rand = Random.Range(0, 4);
        return points[rand];
    }*/
    public Vector3 RandomNavmeshLocation(float radius)
    {
        Vector3 randomDirection = Random.insideUnitSphere * radius;
        randomDirection += transform.position;
        NavMeshHit hit;
        Vector3 finalPosition = Vector3.zero;
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
        {
            finalPosition = hit.position;
        }
        return finalPosition;
    }

    private void Chase(GameObject enemy)
    {
        //inChase = true;
        chase = true;
        target = enemy;
        if (!imDead)
        {
            distance = Vector3.Distance(target.transform.position, this.transform.position);
            if(distance < 25)
            {
                if (target.transform.tag == "Player")
                {
                    anim.SetBool("walk", true);
                    navMess.destination = target.transform.position;
                    distance = Vector3.Distance(target.transform.position, this.transform.position);
                    if (distance < 3)
                    {
                        if (!inAttack)
                        {
                            Attack();
                        }
                    }
                }
                else if(target.transform.tag == "Boar")
                {
                    if (!target.transform.GetComponent<IABoar>().imDead)
                    {
                        anim.SetBool("walk", true);
                        navMess.destination = target.transform.position;
                        distance = Vector3.Distance(target.transform.position, this.transform.position);
                        if(distance < 3)
                        {
                            if (!inAttack)
                            {
                                Attack();
                            }
                        }

                    }
                    else
                    {
                        inAttack = false;
                        chase = false;
                        patrol = true;
                    }
                }
            }
            else
            {
                chase = false;
                patrol = true;
            }
        }
    }

    public void Attack()
    {
        if (Time.time > nextAttackTime)
        {
            inAttack = true;
            nextAttackTime = Time.time + cd;
            animDuration = GetClipLength("Attack(2)");
            StartCoroutine(Damage());
        }
    }

    IEnumerator Damage()
    {
        this.anim.SetTrigger("attack2");
        this.anim.SetBool("walk", false);
        yield return new WaitForSeconds(animDuration/2);
        this.transform.GetChild(1).GetComponent<SphereCollider>().enabled = true;
        inAttack = false;
    }

    private void Load()
    {
        radius = new Vector3(100, 0, 100);
        damageArea = this.transform.GetChild(1).gameObject.transform.GetComponent<SphereCollider>();
        navMess = GetComponent<NavMeshAgent>();
        startPosition = this.transform.position;
        anim = this.transform.GetComponent<Animator>();
        for (int i = 0; i < 5; i++)
        {
            Vector3 newEnemyPosition = this.transform.position + new Vector3(UnityEngine.Random.Range(-radius.x / 2, radius.x / 2), 0, UnityEngine.Random.Range(-radius.z / 2, radius.z / 2));
            points[i] = newEnemyPosition;
        }
    }

    //private void LookAtMe()
    //{

    //    float distance = Vector3.Distance(target, this.transform.position);
    //    print(distance);
    //    Vector3 alFrente = -this.transform.forward;
    //    //if (distance > 50) isFar = true;
    //    //else isFar = false;
    //    //if (distance < 30 && !inChase) Alert(); ;
    //    Quaternion rotacion = Quaternion.FromToRotation(alFrente, (player.transform.position - this.transform.position)) * this.transform.rotation;
    //    this.transform.rotation = Quaternion.Slerp(this.transform.rotation, rotacion, Time.fixedDeltaTime);

    //}


    public void damageThis(float damage, GameObject enemy)
    {
        rand = Random.Range(0, 1);
        this.health -= (damage - armor);
        if(rand == 0) this.anim.SetTrigger("getHit0");
        else this.anim.SetTrigger("getHit");
        Die();
        Chase(enemy);
    }

    private void Die()
    {
        if (health <= 0)
        {
            imDead = true;
            anim.SetTrigger("dead");
            StartCoroutine(Remove());
        }
    }

    IEnumerator Remove()
    {
        animDuration = GetClipLength("Dead");
        yield return new WaitForSeconds(1);
        //target.GetComponent<FPSPlayerStatsAsigner>().getPlayerStats().TalentPoints++;
        Instantiate(meetDrop, new Vector3(this.transform.position.x, this.transform.position.y + 10, this.transform.position.z), Quaternion.identity);
        yield return new WaitForSeconds(2.5f);
        Destroy(this.gameObject);
    }

    public float GetClipLength(string anim)
    {

        RuntimeAnimatorController ac = GetComponent<Animator>().runtimeAnimatorController;
        float duracion = 0f;
        for (int i = 0; i < ac.animationClips.Length; i++)
        {
            if (ac.animationClips[i].name == anim)
            {
                duracion = ac.animationClips[i].length;
            }
        }
        return duracion;
    }
}
