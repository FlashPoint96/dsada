﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAStats : MonoBehaviour
{
    [Header("STATS")]
    public string name;
    public float cdPercent;
    public float cd;
    public float nextAttackTime;
    public float actualHealth;
    public float maxHealth;
    public float armor;
    public float damage;
    public float attackRange;
    public float walkVelocity;
    public float runVelocity;
    public int id;
    private IAAnimations anim;
    public ScriptableIAStats stats;
    public Sun sun;
    private bool nightDone;

    private GameObject player;

    public bool imDead;
    public Vector3 position;
    public bool inPool;
    private GameObject[] dropeableItems;
    private NavmeshController navmeshController;

    private StateMachine stateMachine;

    void Awake()
    {
        player = GameObject.Find("FPSController");
        navmeshController = GetComponent<NavmeshController>();
        sun = FindObjectOfType<Sun>();
        anim = GetComponent<IAAnimations>();
        stateMachine = GetComponentInParent<StateMachine>();
        LoadStats();
    }

    // Update is called once per frame
    void Update()
    {
        Night();
    }

    private void Night()
    {
        if (!sun.inDay)
        {
            this.transform.GetChild(2).transform.localScale = new Vector3(2, 2, 2);
            this.transform.GetChild(3).transform.localScale = new Vector3(2, 2, 2);
        }
        else
        {
            this.transform.GetChild(2).transform.localScale = new Vector3(1, 1, 1);
            this.transform.GetChild(3).transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public void LoadStats()
    {
        this.id = stats.id;
        this.name = stats.name;
        this.cdPercent = stats.cdPercent;
        this.cd = stats.cd;
        this.actualHealth = stats.actualHealth;
        this.maxHealth = stats.maxHealth;
        this.armor = stats.armor;
        this.damage = stats.damage;
        this.inPool = stats.inPool;
        this.position = stats.position;
        this.dropeableItems = stats.dropeableItems;
        this.attackRange = stats.attackRange;
        this.runVelocity = stats.runVelocity;
        this.walkVelocity = stats.walkVelocity;
    }

    public void damageThis(float damage, GameObject enemy)
    {
        if (!imDead)
        {
            this.actualHealth -= (damage - armor);
            print("VIDA " + this.actualHealth);
            StartCoroutine(anim.ChangeAnimOverOthers(anim.ENEMY_GET_HIT));
            Die();
            followPlayer(enemy);
        }
    }

    private void followPlayer(GameObject enemy)
    {
        float distance = Vector3.Distance(enemy.transform.position, this.transform.position);
        if (distance > 20)
        {
            navmeshController.UpdateDestination(enemy.transform.position);
        }
        else
        {
            stateMachine.ChangeState(stateMachine.chaseState);
        }
    }

    private void Die()
    {
        if (actualHealth <= 0)
        {
            StartCoroutine(anim.ChangeAnimOverOthers(anim.ENEMY_DEAD));
            //print("Me muero");
            imDead = true;
            StartCoroutine(Remove());
        }
    }

    IEnumerator Remove()
    {
        yield return new WaitForSeconds(anim.getAnimDuration());
        for (int j = 0; j < UnityEngine.Random.Range(2, 6); j++)
        {
            Instantiate(dropeableItems[UnityEngine.Random.Range(0, 6)], new Vector3(this.transform.position.x, this.transform.position.y + 0.5f, this.transform.position.z), Quaternion.identity);
        }
        if(UnityEngine.Random.Range(0,10) == 3)
        {
            player.GetComponent<FPSPlayerStatsAsigner>().fps.TalentPoints++;
        }
        //print("Vuelvo a la pool"); 
        if(stats.id == 4)
        {
            Destroy(this.gameObject);
        }
        else
        {
            GameObject go1 = new GameObject();
            go1.name = "REMOVE";
            EnemySpawner.instance.returnEnemyAndDelete(this.gameObject, go1);
        }
    }
}
