﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScriptableIAStats : ScriptableObject
{
    public Vector3 position;
    public bool inPool;
    public string name;
    public float cdPercent;
    public float cd;
    public float actualHealth;
    public float maxHealth;
    public float armor;
    public float damage;
    public float attackRange;
    public float walkVelocity;
    public float runVelocity;
    public int id;
    public GameObject[] dropeableItems;
}
