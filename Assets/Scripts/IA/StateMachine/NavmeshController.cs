﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;
public class NavmeshController : MonoBehaviour
{
    public Transform target;
    private IAAnimations anim;

    public LayerMask ignoreEnemy;

    private NavMeshAgent navMeshAgent;
    private StateMachine stateMachine;

    void Awake()
    {
        stateMachine = GetComponent<StateMachine>();
        anim = GetComponent<IAAnimations>();
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    public void UpdateDestination(Vector3 point)
    {       
        if (navMeshAgent == null) navMeshAgent = GetComponent<NavMeshAgent>();
        if(stateMachine.actualState != stateMachine.chaseState) anim.ChangeAnim(anim.ENEMY_WALK);

        navMeshAgent.SetDestination(point);
        navMeshAgent.isStopped = false;
    }
    public void UpdateDestinationChase(Vector3 point)
    {

        if (navMeshAgent == null) navMeshAgent = GetComponent<NavMeshAgent>();
        if (stateMachine.actualState != stateMachine.attackState) anim.ChangeAnim(anim.ENEMY_RUN);
        //anim.SetBool("shout", false);
        //anim.SetBool("walk", true);
        navMeshAgent.SetDestination(point);
        navMeshAgent.isStopped = false;
    }
    public void UpdateDestination()
    {
        if (navMeshAgent == null) navMeshAgent = GetComponent<NavMeshAgent>();
        if (stateMachine.actualState != stateMachine.chaseState) anim.ChangeAnim(anim.ENEMY_WALK);
        //anim.SetBool("shout", false);
        //anim.SetBool("walk", true);
        UpdateDestination(target.position);
    }

    public void StopAgent()
    {
        if (navMeshAgent == null) navMeshAgent = GetComponent<NavMeshAgent>();
        if (stateMachine.actualState != stateMachine.attackState) anim.ChangeAnim(anim.ENEMY_SHOUT);
        navMeshAgent.isStopped = true;
    }
    public void StopAgentAttack()
    {
        if (navMeshAgent == null) navMeshAgent = GetComponent<NavMeshAgent>();
        //if (stateMachine.actualState != stateMachine.attackState) anim.ChangeAnim(anim.ENEMY_SHOUT);
        //print(anim);
        //anim.SetBool("walk", false);
        navMeshAgent.isStopped = true;
    }
    public bool Arrived()
    {
        //print(navMeshAgent.remainingDistance + " " + navMeshAgent.stoppingDistance + " " + navMeshAgent.pathPending);
        return navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance && !navMeshAgent.pathPending;
    }

    public NavMeshAgent GetNavMeshAgent()
    {
        return navMeshAgent;
    }

    public void SetTarget(Transform transf)
    {
        this.target = transf;
    }
}