﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : MonoBehaviour
{
    private IAStats iaStats;
    private StateMachine stateMachine;
    private NavmeshController navController;
    private VisionController visionController;
    private IAAnimations anim;
    public Color colorState = Color.black;
    public float nextAttackTime;
    public bool inAttack;
    private float distance;

    void Awake()
    {
        anim = GetComponent<IAAnimations>();
        iaStats = GetComponent<IAStats>();
        stateMachine = GetComponent<StateMachine>();
        navController = GetComponent<NavmeshController>();
        visionController = GetComponent<VisionController>();
    }

    void OnEnable()
    {
        stateMachine.MeshRenderer.material.color = colorState;
        stateMachine.light.color = colorState;
    }
    void Update()
    {
        Attack();
    }

    public void Attack()
    {
        distance = Vector3.Distance(visionController.GetFocus().transform.position, this.transform.position);

        if (distance < iaStats.attackRange)
        {
            navController.StopAgentAttack();

            print("en distancia de ataque");
            if (Time.time > nextAttackTime && !inAttack)
            {
                print("atacando!");
                inAttack = true;
                nextAttackTime = Time.time + iaStats.cd;
                StartCoroutine(Damage());
            }
        }
        else stateMachine.ChangeState(stateMachine.chaseState);
    }

    IEnumerator Damage()
    {
        anim.ChangeAnim(anim.ENEMY_ATTACK);
        yield return new WaitForSeconds(anim.getAnimDuration());
        print("haciendo daño");
        this.transform.GetChild(1).GetComponent<SphereCollider>().enabled = true;
        inAttack = false;
    }
}
