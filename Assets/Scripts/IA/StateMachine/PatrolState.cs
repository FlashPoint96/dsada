using UnityEngine;
using System.Collections;
using UnityEngine.AI;
public class PatrolState : MonoBehaviour 
{

    public Transform[] WayPoints;
    public Color colorState = Color.green;

    private IAStats iaStats;
    private StateMachine stateMachine;
    private NavmeshController navController;
    private VisionController visionController;
    private int nextWayPoint;
    

    void Awake()
    {
        iaStats = GetComponent<IAStats>();
        stateMachine = GetComponent<StateMachine>();
        navController = GetComponent<NavmeshController>();
        visionController = GetComponent<VisionController>();
    }
	
	void Update ()
    {
        if (navController.Arrived())
        {
            //print("He llegado");
            stateMachine.ChangeState(stateMachine.alertState);
        }
	}

    void OnEnable()
    {
        stateMachine.MeshRenderer.material.color = colorState;
        stateMachine.light.color = colorState;
        navController.GetNavMeshAgent().speed = iaStats.walkVelocity;
        UpdatePoint();
    }

    void UpdatePoint()
    {
        //print("Voy a " + nextWayPoint);
        if(iaStats.id == 4)
        {
            nextWayPoint = Random.Range(0, 5);
            navController.UpdateDestination(WayPoints[nextWayPoint].position);
        }
        else navController.UpdateDestination(RandomNavmeshLocation(50));
    }

    public Vector3 RandomNavmeshLocation(float radius)
    {
        Vector3 randomDirection = Random.insideUnitSphere * radius;
        randomDirection += transform.position;
        NavMeshHit hit;
        Vector3 finalPosition = Vector3.zero;
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
        {
            finalPosition = hit.position;
        }
        return finalPosition;
    }
}
