﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuspectState : MonoBehaviour
{
    public Color colorState = Color.blue;
    public Color colorCantSee = Color.white;
    private StateMachine stateMachine;
    private NavmeshController navController;
    private VisionController visionController;
    private NoiseController noiseController;
    private IAStats iaStats;


    private Animator anim;
    private float time;
    private float limitTime = 3;
    private float bombTime = 5;

    void Awake()
    {
        anim = GetComponent<Animator>();
        stateMachine = GetComponent<StateMachine>();
        navController = GetComponent<NavmeshController>();
        visionController = GetComponent<VisionController>();
        noiseController = GetComponentInChildren<NoiseController>(); 
        iaStats = GetComponent<IAStats>();
    }

    void Update()
    {
        if(noiseController.GetTargetPosition() != null)
        {
            float distance = Vector3.Distance(noiseController.GetTargetPosition().position, this.transform.position);
            time += Time.deltaTime;

            if (noiseController.GetTargetPosition().tag == "Distraction")
            {
                //if(time > bombTime) stateMachine.ChangeState(stateMachine.alertState);
                if (distance <= iaStats.attackRange)
                {
                    print("Distance if " + distance);
                    navController.StopAgent();
                }
                else
                {
                    print("Distance else " + distance + " es " + noiseController.GetTargetPosition().tag);
                    navController.GetNavMeshAgent().speed = iaStats.runVelocity;
                    navController.UpdateDestination(noiseController.GetTargetPosition().position);
                }
                
               // navController.UpdateDestination(noiseController.GetTargetPosition().position);

            }
            else if (noiseController.GetTargetPosition().tag == "Player")
            {
                if (iaStats.id == 4)
                {
                    navController.UpdateDestination(noiseController.GetTargetPosition().position);
                }
                else
                {
                    print("Te oigo pero no te veo");
                    stateMachine.MeshRenderer.material.color = colorCantSee;
                    stateMachine.light.color = colorCantSee;
                    navController.StopAgent();
                    if (time > limitTime) stateMachine.ChangeState(stateMachine.alertState);
                }
            }
        }
        else
        {
            stateMachine.ChangeState(stateMachine.alertState);
        }
    }

    void OnEnable()
    {
        stateMachine.MeshRenderer.material.color = colorState;
        stateMachine.light.color = colorState;
        time = 0;
        //navController.StopAgent();
        LookAtMe(noiseController.GetTargetPosition().position);
    }
    private void LookAtMe(Vector3 target)
    {
        Quaternion rotacion = Quaternion.FromToRotation(this.transform.forward, (target - this.transform.position)) * this.transform.rotation;
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, rotacion, Time.fixedDeltaTime);
    }
}
