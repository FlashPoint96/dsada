﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseController : MonoBehaviour
{
    private VisionController visionController;
    private StateMachine stateMachine;
    private NavmeshController navMeshController;

    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController controller;

    private float distance;
    private float playerNoise;
    private float amountNoise;
    private Transform targetPosition;
    void Awake()
    {
        controller = GameObject.FindObjectOfType<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();
        visionController = GetComponentInParent<VisionController>();
        stateMachine = GetComponentInParent<StateMachine>();
        navMeshController = GetComponentInParent<NavmeshController>();
    }
    private void OnTriggerStay(Collider other)
    {

        //print("Oigo algo... " + other.transform.tag);
        distance = Vector3.Distance(stateMachine.GetPlayer().transform.position, this.transform.position);
        if(other.tag == "Distraction")
        {
            print("Bomba");
            targetPosition = other.gameObject.transform;
            stateMachine.ChangeState(stateMachine.suspectState);
        }
        else if (other.tag == "Player")
        {
            print("Player");

            playerNoise = other.GetComponent<FPSPlayerStatsAsigner>().GetNoise();
            amountNoise = distance - playerNoise;
            //print(" X: " + amountNoise);

            if (amountNoise < 0 && stateMachine.actualState != stateMachine.chaseState)
            {
                targetPosition = other.gameObject.transform;
                stateMachine.ChangeState(stateMachine.suspectState);
            }
        }
    }

    public Transform GetTargetPosition()
    {
        return targetPosition;
    }

  /*  IEnumerator Check(Transform transf)
    {
        navMeshController.StopAgent();
        LookAtMe(transf.position);
        yield return new WaitForSeconds(2);
        print("Llamo cansee desde NoiseController");
        visionController.CanSee(transf.transform);
    }

    private void LookAtMe(Vector3 target)
    {
        Quaternion rotacion = Quaternion.FromToRotation(this.transform.forward, (target - this.transform.position)) * this.transform.rotation;
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, rotacion, Time.fixedDeltaTime);

    }*/
}
