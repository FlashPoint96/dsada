using UnityEngine;
using System.Collections;

public class StateMachine : MonoBehaviour 
{
    public MonoBehaviour patrolState;
    public MonoBehaviour alertState;
    public MonoBehaviour chaseState;
    public MonoBehaviour initialState;
    public MonoBehaviour attackState;
    public MonoBehaviour suspectState;

    public MeshRenderer MeshRenderer;
    public Light light;
    private GameObject player;

    public MonoBehaviour actualState;

    private IAStats iaStats;

    void Awake()
    {
        iaStats = GetComponent<IAStats>();
        player = GameObject.Find("FPSController");
        patrolState = GetComponent<PatrolState>();
        alertState = GetComponent<AlertState>();
        chaseState = GetComponent<ChaseState>();
        attackState = GetComponent<AttackState>();
        suspectState = GetComponent<SuspectState>();
        initialState = patrolState;
    }
    void Start () 
    {
        ChangeState(initialState);
	}
	
    public void ChangeState(MonoBehaviour newState)
    {
        if (!iaStats.imDead)
        {
            if (actualState != null) actualState.enabled = false;
            actualState = newState;
            actualState.enabled = true;
        }   
    }

    public void OffMachine()
    {
        actualState.enabled = false;
    }

    public GameObject GetPlayer()
    {
        return player;
    }
}
