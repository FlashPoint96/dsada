using UnityEngine;
using System.Collections;
using UnityEngine.AI;
public class VisionController : MonoBehaviour {

    public Transform eyes;
    public float visionRange = 60f;
    public Vector3 offset = new Vector3(0f, 0.75f, 0f);
    private GameObject focus;

    private NavmeshController navController;
    private StateMachine stateMachine;

    public bool inCanSee;


    void Awake()
    {
        navController = GetComponent<NavmeshController>();
        stateMachine = GetComponent<StateMachine>();
        focus = stateMachine.GetPlayer();
    }
    public GameObject GetFocus()
    {
        return focus;
    }
    public void CanSee(Transform target)
    {
        if (!inCanSee)
        {
            inCanSee = true;
            //print("Entro en canSee " + target.name);
            RaycastHit hit;
            if (Physics.Raycast(eyes.position, (target.transform.position - eyes.position) + offset, out hit, visionRange))
            {
                focus = hit.transform.gameObject;
                //print(hit.transform.tag);
                Debug.DrawRay(eyes.position, target.transform.position - eyes.position, Color.red, 5f);
                float distance = Vector3.Distance(target.transform.position, this.transform.position);
                //print(distance);
               // print("CanSee ve a " + hit.transform.tag);
               // print("Name " + hit.transform.name);
                if (hit.transform.tag == "Player")
                {
                    //print("Cambiando estado a Chase");
                    stateMachine.ChangeState(stateMachine.chaseState);
                }
            
            }
           // else print("No hay ray");
            inCanSee = false;
        }
    }

    private void LookAtMe(Vector3 target)
    {
        Quaternion rotacion = Quaternion.FromToRotation(this.transform.forward, (target - this.transform.position)) * this.transform.rotation;
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, rotacion, Time.fixedDeltaTime);

    }


}
