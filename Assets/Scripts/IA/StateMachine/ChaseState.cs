using UnityEngine;
using System.Collections;

public class ChaseState : MonoBehaviour
{

    public Color colorState = Color.red;

    private IAStats iaStats;
    private StateMachine stateMachine;
    private NavmeshController navController;
    private VisionController visionController;

    private IAAnimations anim;

    private int meleRange = 3;

	void Awake () 
    {
        anim = this.GetComponent<IAAnimations>();
        iaStats = GetComponent<IAStats>();
        stateMachine = GetComponent<StateMachine>();
        navController = GetComponent<NavmeshController>();
        visionController = GetComponent<VisionController>();

	}

    void OnEnable()
    {
        stateMachine.MeshRenderer.material.color = colorState;
        stateMachine.light.color = colorState;
        navController.GetNavMeshAgent().speed = iaStats.runVelocity;
    }

    void Update () 
    {
        float distance = Vector3.Distance(visionController.GetFocus().transform.position, this.transform.position);
        //print("Distance: " + distance);
        if (distance < visionController.visionRange + 5)
        {
            //print("Estoy en Chase,persiguiendo!");
            if (distance < iaStats.attackRange)
            {
                stateMachine.ChangeState(stateMachine.attackState);
            }
            else
            {
                if (visionController.GetFocus().transform.tag == "Player")
                {
                    LookAtMe(visionController.GetFocus().transform.position);
                    print("GetPlayer es: " + stateMachine.GetPlayer());
                    navController.UpdateDestinationChase(stateMachine.GetPlayer().transform.position);
                }
                else stateMachine.ChangeState(stateMachine.alertState);
            }
        }
        else
        {
            //print("Cambio ALERTA");
            stateMachine.ChangeState(stateMachine.alertState);
        }
        //Physics.SyncTransforms();
	}

    private void LookAtMe(Vector3 target)
    {
        Quaternion rotacion = Quaternion.FromToRotation(this.transform.forward, (target - this.transform.position)) * this.transform.rotation;
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, rotacion, Time.fixedDeltaTime);
    }
}
