﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vision : MonoBehaviour
{
    private VisionController visionController;
    private StateMachine stateMachine;

    void Awake()
    {
        visionController = GetComponentInParent<VisionController>();
        stateMachine = GetComponentInParent<StateMachine>();
    }
    private void OnTriggerStay(Collider other)
    {
        //print("veo " + other.transform.tag);
        if (other.tag == "Player")
        {
            //print("Llamo cansee");

            //print("Cambio state a alert");

            //stateMachine.ChangeState(stateMachine.alertState);
            visionController.CanSee(other.transform);
        }

        if (other.tag == "Boar")
        {

        }
    }
}
