using UnityEngine;
using System.Collections;

public class AlertState : MonoBehaviour 
{

    public float velocity = 3;
    public float duration;
    public Color colorState = Color.yellow;

    private StateMachine stateMachine;
    private NavmeshController navController;
    private VisionController visionController;
    private float time;
    private bool looking = false;
    private IAAnimations anim;

	void Awake () 
    {
        anim = GetComponent<IAAnimations>();
        stateMachine = GetComponent<StateMachine>();
        navController = GetComponent<NavmeshController>();
        visionController = GetComponent<VisionController>();
	}

    void OnEnable()
    {
        stateMachine.MeshRenderer.material.color = colorState;
        stateMachine.light.color = colorState;
        navController.StopAgent();
        time = 0f;
        duration = Random.Range(5, 10);
    }

    void Update()
    {
        if (!enabled) return;
        if (!visionController.inCanSee)
        {
            time += Time.deltaTime;
            if (!looking) StartCoroutine(Search());


            if (time >= duration)
                if (stateMachine.actualState == this)
                {
                    stateMachine.ChangeState(stateMachine.patrolState);
                }
        }
    }

    IEnumerator Search()
    {
        looking = true;

        print("Entro en corutina");
        //Vector3 deltaAngle = new Vector3(0, Random.Range(-180, 180f), 0);
        //for (float t = 0; t < 1; t += Time.deltaTime / Random.Range(2, 5))
        //{
        //    transform.Rotate(deltaAngle * Time.deltaTime);
        //    yield return null;
        //}
        transform.Rotate(new Vector3(0, Random.Range(-180, 180f), 0));

        yield return new WaitForSeconds(Random.Range(2, 5));
        print("Acabo de rotar");
        looking = false;
    }

}
