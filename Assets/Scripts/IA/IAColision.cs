﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAColision : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //print("Que veo?  " + other.transform.tag);
        if (other.tag == "Player")
        {
            GetComponentInParent<IATest>().target = other.gameObject;
            GetComponentInParent<IATest>().chase = true;
        }

        if (other.tag == "Boar")
        {
            if(!GetComponentInParent<IATest>().chase)
            {
                if(Random.Range(1, 10) > 6)
                {
                    GetComponentInParent<IATest>().target = other.gameObject;
                    GetComponentInParent<IATest>().chase = true;
                }
            }
            else
            {
                GetComponentInParent<IATest>().target = other.gameObject;
                GetComponentInParent<IATest>().chase = true;
            }
        }
    }
}
