﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageAreaMechanic : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //print(other.tag);
        print("ENTRO EN COLISION CON " + other.name + "y soy el objeto" +this.gameObject.name);
        if(other.tag == "Player")
        {
            other.gameObject.GetComponent<FPSPlayerStatsAsigner>().damageThis(this.GetComponentInParent<IAStats>().damage);
            print("apago collider");
            this.GetComponent<SphereCollider>().enabled = false;
        }
        if(other.tag == "Boar")
        {
            other.transform.GetComponent<IABoar>().health -= this.GetComponentInParent<IATest>().damage;
            GetComponentInParent<IATest>().hitBoar = true;
            print("apago collider");
            this.GetComponent<SphereCollider>().enabled = false;
        }
    }
    //private void OnTriggerExit(Collider other)
    //{
    //    if (other.tag == "Player")
    //    {
    //        GetComponentInParent<IATest>().hitPlayer = false;
    //    }
    //    if (other.tag == "Boar")
    //    {
    //        GetComponentInParent<IATest>().hitBoar = false;
    //    }
    //}
}
