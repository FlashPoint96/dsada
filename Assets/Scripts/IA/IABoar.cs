﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IABoar : MonoBehaviour
{
    private Animator anim;
    private float animDuration;

    public GameObject meetDrop;

    [Header("STATS")]
    public float health;

    [Header("STATS")]
    public bool imDead = false;
    void Start()
    {
        anim = this.transform.GetComponent<Animator>();
    }
    
    // Update is called once per frame
    void Update()
    {
        Die();
    }

    private void Die()
    {
        if (!imDead)
        {
            if(health <= 0)
            {
                imDead = true;
                anim.SetBool("die", true);
                StartCoroutine(Remove());
            }
        }
    }

    public void damageThis(float damage)
    {
        this.health -= damage;
        Die();
    }

    IEnumerator Remove()
    {
        animDuration = GetClipLength("die");

        yield return new WaitForSeconds(1);
        Instantiate(meetDrop, new Vector3(this.transform.position.x, this.transform.position.y + 5, this.transform.position.z), Quaternion.identity);
        yield return new WaitForSeconds(2.5f);
        Destroy(this.gameObject);
    }

    public float GetClipLength(string anim)
    {

        RuntimeAnimatorController ac = GetComponent<Animator>().runtimeAnimatorController;
        float duracion = 0f;
        for (int i = 0; i < ac.animationClips.Length; i++)
        {
            if (ac.animationClips[i].name == anim)
            {
                duracion = ac.animationClips[i].length;
            }
        }
        return duracion;
    }
}
