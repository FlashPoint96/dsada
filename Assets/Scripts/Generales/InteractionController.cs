﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionController : MonoBehaviour
{
    private Camera camera;
    private Vector3 door;
    private string tag;
    private GameObject enemySpawner;
    private IAStats enemy;
    public GameObject[] dropeableObjects;
    public AudioClip clipLight;
    public AudioClip clipDoor;
    public AudioClip clipBox;
    public AudioClip clipr;
    public GameObject mp5;
    public GameObject m2;
    public GameObject gun;
    public GameObject bullets;
    void Start()
    {
        enemySpawner = GameObject.Find("EnemySpawnController");
        camera = this.transform.GetChild(0).gameObject.GetComponent<Camera>();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            InteractDoor();
            enemy = DetectEnemy();
        }
    }
    private void InteractDoor()
    {
        RaycastHit[] hits;
        hits = Physics.RaycastAll(camera.transform.position, camera.transform.forward, 4);
        for (int i = 0; i < hits.Length; i++)
        {
            RaycastHit hit = hits[i];
            tag = hit.transform.gameObject.tag;
            print("TANKI " + tag);
            Debug.DrawRay(camera.transform.position, hit.point, Color.yellow, 5f);
            switch (tag)
            {
                case "DoorA":
                    AudioSource.PlayClipAtPoint(clipDoor, hit.transform.position);
                    print("DoorA" + hit.transform.gameObject.tag);
                    if (hit.transform.rotation.y != 0)
                    {
                        door = hit.transform.rotation.eulerAngles;
                        door.y = 0f;
                        hit.transform.rotation = Quaternion.Euler(door);
                    }
                    else
                    {
                        door = hit.transform.rotation.eulerAngles;
                        door.y = -90f;
                        hit.transform.rotation = Quaternion.Euler(door);
                    }
                    break;
                case "DoorB":
                    AudioSource.PlayClipAtPoint(clipDoor, hit.transform.position);
                    if (hit.transform.rotation.y != 0)
                    {
                        door = hit.transform.rotation.eulerAngles;
                        door.y = 0f;
                        hit.transform.rotation = Quaternion.Euler(door);
                    }
                    else
                    {
                        door = hit.transform.rotation.eulerAngles;
                        door.y = 90f;
                        hit.transform.rotation = Quaternion.Euler(door);
                    }
                    break;
                case "DoorC":
                    AudioSource.PlayClipAtPoint(clipDoor, hit.transform.position);
                    if (hit.transform.rotation.y != 0)
                    {
                        door = hit.transform.rotation.eulerAngles;
                        door.y = 0f;
                        hit.transform.rotation = Quaternion.Euler(door);
                    }
                    else
                    {
                        door = hit.transform.rotation.eulerAngles;
                        door.y = 120f;
                        hit.transform.rotation = Quaternion.Euler(door);
                    }
                    break;
                case "DoorD":
                    AudioSource.PlayClipAtPoint(clipDoor, hit.transform.position);
                    if (hit.transform.rotation.y != 180)
                    {
                        door = hit.transform.rotation.eulerAngles;
                        door.y = 180f;
                        hit.transform.rotation = Quaternion.Euler(door);
                    }
                    else
                    {
                        door = hit.transform.rotation.eulerAngles;
                        door.y = 65f;
                        hit.transform.rotation = Quaternion.Euler(door);
                    }
                    break;
                case "Lamp":
                    AudioSource.PlayClipAtPoint(clipLight, hit.transform.position);
                    if (hit.transform.parent.transform.GetChild(2).GetComponent<Light>().enabled)
                    {

                        hit.transform.parent.transform.GetChild(2).GetComponent<Light>().enabled = false;
                    }
                    else
                    {
                        hit.transform.parent.transform.GetChild(2).GetComponent<Light>().enabled = true;
                    }
                    break;
                case "ActiveSecureZone":
                    if (enemySpawner.GetComponent<EnemySpawner>().tryToActiveZone(hit.transform.gameObject.GetComponent<ActiveSecureZone>().getZoneName()))
                    {
                        hit.transform.gameObject.GetComponent<ActiveSecureZone>().changeLightToActive();
                    }
                    break;
                case "Box":

                    if (!hit.transform.gameObject.GetComponent<BoxController>().isEmpty)
                    {
                        hit.transform.gameObject.GetComponent<BoxController>().isEmpty = true;
                        if(Random.Range(1, 11) > 5)
                        {
                            AudioSource.PlayClipAtPoint(clipBox, hit.transform.position);
                            for (int j = 0; j < Random.Range(2, 6); j++)
                            {
                                Instantiate(dropeableObjects[Random.Range(0, 9)], this.transform.position + (this.transform.forward * 1.4f), Quaternion.identity);
                            }
                        }

                    }
                    break;
                case "BoxBoss":
                    Instantiate(m2, this.transform.position + (this.transform.forward * 1.2f), Quaternion.identity);
                    AudioSource.PlayClipAtPoint(clipBox, hit.transform.position);
                    break;
                case "BoxGun":
                    Instantiate(gun, this.transform.position + (this.transform.forward * 1.2f), Quaternion.identity);
                    AudioSource.PlayClipAtPoint(clipBox, hit.transform.position);
                    break;
                case "BoxMp5":
                    Instantiate(mp5, this.transform.position + (this.transform.forward * 1.2f), Quaternion.identity);
                    AudioSource.PlayClipAtPoint(clipBox, hit.transform.position);
                    break;
                case "BoxExtra":
                    Instantiate(bullets, this.transform.position + (this.transform.forward * 1.2f), Quaternion.identity);
                    AudioSource.PlayClipAtPoint(clipBox, hit.transform.position);
                    break;
            }

        }

    }

    public IAStats DetectEnemy()
    {
        IAStats enemigo;
        int rango = 30;
        RaycastHit hit;
        if (Physics.Raycast(camera.transform.position, camera.transform.forward * rango, out hit, rango))
        {

            Debug.DrawRay(camera.transform.position, hit.point, Color.red, 5f);
            if (hit.collider.tag == "Enemy")
            {

                enemigo = hit.collider.gameObject.GetComponent<IAStats>();
                print(enemigo.name+"SOY ENEMIGOSASADA");
                return enemigo;
            }


        }

        return null;
    }

    public IAStats getEnemyTarget()
    {
        return enemy;

    }
    public void setEnemyTarget(IAStats enemigo)
    {
        print("SE ASIGNADO UN ENEMIGO " + enemigo);
        this.enemy = enemigo;

    }

    private void OnCollisionEnter(Collision collision)
    {
        print("HE COLISIONADO CON "+collision.gameObject.tag);
        if (collision.gameObject.tag == "item")
        {
            var item = collision.transform.gameObject.GetComponent<itemComponent>();
            if (item)
            {
                //print(item.getThisItem());
                if (this.GetComponentInChildren<InventoryController>().getInventoryItem().Slot.Count != 24)
                {

                    this.GetComponentInChildren<InventoryController>().getInventoryItem().AddItem(new ItemObject(item.getThisItem()), 1);
                    GameObject.Destroy(collision.transform.gameObject);
                }
            }
        }
    }
}
