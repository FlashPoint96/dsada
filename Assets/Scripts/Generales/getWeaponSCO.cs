﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getWeaponSCO : MonoBehaviour
{
    [SerializeField]
    private WeaponScriptableObject WSO;

    public WeaponScriptableObject getWeapon()
    {
        return WSO;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            float totalDamage = this.GetComponentInParent<FPSPlayerStatsAsigner>().getPlayerStats().Damage + WSO.damage;
            print("Le haria un daño de al enemigo" + totalDamage);
        }
    }
}
