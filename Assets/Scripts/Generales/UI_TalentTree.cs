﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UI_TalentTree : MonoBehaviour
{
    private List<playerTalents.Talents> talentsList;
    private FPSPlayerStats FPSPlayerStats;
    public GameObject talentPointsText;
    public AllTreeObjects[] talentTreeButons;

    [System.Serializable]
    public class AllTreeObjects
    {
        public string nameOfTreeTalent;
        public ObjetoTalentos[] objectTalents;
    }

    [System.Serializable]
    public class ObjetoTalentos
    {
        public string TalentName;
        public GameObject button;
        public GameObject lineToUpperTalent;
    }

    public GameEvent talentEvent;
    private bool H1, H2, H3,A1,A2,A3,C1,C2;
    private void Awake()
    {
        FPSPlayerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<FPSPlayerStatsAsigner>().getPlayerStats();
        UpdateTalentPointsText();
        this.SetPlayerTalents(FPSPlayerStats.TalentsUnlocked);
    }
    private void FixedUpdate()
    {
        UpdateTalentPointsText();
    }
    public void SetPlayerTalents(playerTalents playerTalent)
    {
        //print(playerTalent.getTalents());


        foreach (playerTalents.Talents t in playerTalent.getTalents())
        {
            //print("TALENTO CON NOMBRE DE LO TIENE DESBLOQUEADO "+t);
            switch (t)
            {
                case playerTalents.Talents.HealthLv1:
                    talentTreeButons[0].objectTalents[0].button.gameObject.GetComponent<Image>().color = Color.red;
                    talentTreeButons[0].objectTalents[0].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                    break;
                case playerTalents.Talents.HealthLv2:
                    talentTreeButons[0].objectTalents[1].button.gameObject.GetComponent<Image>().color = Color.red;
                    talentTreeButons[0].objectTalents[1].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                    break;
                case playerTalents.Talents.HealthLv3:
                    talentTreeButons[0].objectTalents[2].button.gameObject.GetComponent<Image>().color = Color.red;
                    break;
                case playerTalents.Talents.AttackLv1:
                    talentTreeButons[1].objectTalents[0].button.gameObject.GetComponent<Image>().color = Color.red;
                    talentTreeButons[1].objectTalents[0].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                    break;
                case playerTalents.Talents.AttackLv2:
                    talentTreeButons[1].objectTalents[1].button.gameObject.GetComponent<Image>().color = Color.red;
                    talentTreeButons[1].objectTalents[1].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                    break;
                case playerTalents.Talents.AttackLv3:
                    talentTreeButons[1].objectTalents[2].button.gameObject.GetComponent<Image>().color = Color.red;
                    break;
                case playerTalents.Talents.CraftingLv1:
                    talentTreeButons[2].objectTalents[0].button.gameObject.GetComponent<Image>().color = Color.red;
                    talentTreeButons[2].objectTalents[0].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                    break;
                case playerTalents.Talents.CraftingLv2:
                    talentTreeButons[2].objectTalents[1].button.gameObject.GetComponent<Image>().color = Color.red;
                    talentTreeButons[2].objectTalents[1].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                    break;
            }
        }
    }

    public void NewTalent(string talent)
    {
        foreach (playerTalents.Talents t in FPSPlayerStats.TalentsUnlocked.getTalents())
        {
            switch (t)
            {
                case playerTalents.Talents.HealthLv1:
                    talentTreeButons[0].objectTalents[0].button.gameObject.GetComponent<Image>().color = Color.red;
                    talentTreeButons[0].objectTalents[0].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                    break;
                case playerTalents.Talents.HealthLv2:
                    talentTreeButons[0].objectTalents[1].button.gameObject.GetComponent<Image>().color = Color.red;
                    talentTreeButons[0].objectTalents[1].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                    break;
                case playerTalents.Talents.HealthLv3:
                    talentTreeButons[0].objectTalents[2].button.gameObject.GetComponent<Image>().color = Color.red;
                    break;
                case playerTalents.Talents.AttackLv1:
                    talentTreeButons[1].objectTalents[0].button.gameObject.GetComponent<Image>().color = Color.red;
                    talentTreeButons[1].objectTalents[0].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                    break;
                case playerTalents.Talents.AttackLv2:
                    talentTreeButons[1].objectTalents[1].button.gameObject.GetComponent<Image>().color = Color.red;
                    talentTreeButons[1].objectTalents[1].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                    break;
                case playerTalents.Talents.AttackLv3:
                    talentTreeButons[1].objectTalents[2].button.gameObject.GetComponent<Image>().color = Color.red;
                    break;
                case playerTalents.Talents.CraftingLv1:
                    talentTreeButons[2].objectTalents[0].button.gameObject.GetComponent<Image>().color = Color.red;
                    talentTreeButons[2].objectTalents[0].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                    break;
                case playerTalents.Talents.CraftingLv2:
                    talentTreeButons[2].objectTalents[1].button.gameObject.GetComponent<Image>().color = Color.red;
                    talentTreeButons[2].objectTalents[1].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                    break;
            }
        }
        //Lo pongo en 0 para las pruebas
        if (FPSPlayerStats.TalentPoints >= 1)
        {
            switch (talent)
            {
                case "Health1":
                    if (!H1)
                    {
                        if (FPSPlayerStats.TalentsUnlocked.AlreadyUnlockedTalent(playerTalents.Talents.HealthLv1))
                        {
                            print("Ya lo tiene desbloqueado");
                        }
                        else if (FPSPlayerStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.HealthLv1))
                        {
                            talentEvent.RaiseTalent(playerTalents.Talents.HealthLv1);
                            talentTreeButons[0].objectTalents[0].button.gameObject.GetComponent<Image>().color = Color.red;
                            talentTreeButons[0].objectTalents[0].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                            FPSPlayerStats.TalentPoints--;
                            UpdateTalentPointsText();
                        }
                        H1 = true;
                    }
                    break;
                case "Attack1":
                    if (!A1)
                    {
                        if (FPSPlayerStats.TalentsUnlocked.AlreadyUnlockedTalent(playerTalents.Talents.AttackLv1))
                        {
                            print("Ya lo tiene desbloqueado");
                        }
                        else if (FPSPlayerStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.AttackLv1))
                        {
                            talentEvent.RaiseTalent(playerTalents.Talents.AttackLv1);
                            talentTreeButons[1].objectTalents[0].button.gameObject.GetComponent<Image>().color = Color.red;
                            talentTreeButons[1].objectTalents[0].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                            FPSPlayerStats.TalentPoints--;
                            UpdateTalentPointsText();
                        }
                        A1 = true;
                    }
                    break;
                case "Crafting1":
                    if (!C1)
                    {
                        if (FPSPlayerStats.TalentsUnlocked.AlreadyUnlockedTalent(playerTalents.Talents.CraftingLv1))
                        {
                            print("Ya lo tiene desbloqueado");
                        }
                        else if (FPSPlayerStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.CraftingLv1))
                        {
                            talentEvent.RaiseTalent(playerTalents.Talents.CraftingLv1);
                            talentTreeButons[2].objectTalents[0].button.gameObject.GetComponent<Image>().color = Color.red;
                            talentTreeButons[2].objectTalents[0].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                            FPSPlayerStats.TalentPoints--;
                            UpdateTalentPointsText();
                        }
                        C1 = true;
                    }
                    break;
            }
        }
        if (FPSPlayerStats.TalentPoints >= 2)
        {
            switch (talent)
            {
                case "Health2":
                    if (!H2)
                    {
                        if (FPSPlayerStats.TalentsUnlocked.AlreadyUnlockedTalent(playerTalents.Talents.HealthLv2))
                        {
                            print("Ya lo tiene desbloqueado");
                        }
                        else if (FPSPlayerStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.HealthLv2))
                        {
                            talentEvent.RaiseTalent(playerTalents.Talents.HealthLv2);
                            talentTreeButons[0].objectTalents[1].button.gameObject.GetComponent<Image>().color = Color.red;
                            talentTreeButons[0].objectTalents[1].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                            FPSPlayerStats.TalentPoints-=2;
                            UpdateTalentPointsText();
                        }
                        H2 = true;
                    }
                    break;
                case "Attack2":
                    if (!A2)
                    {
                        if (FPSPlayerStats.TalentsUnlocked.AlreadyUnlockedTalent(playerTalents.Talents.AttackLv2))
                        {
                            print("Ya lo tiene desbloqueado");
                        }
                        else if (FPSPlayerStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.AttackLv2))
                        {
                            talentEvent.RaiseTalent(playerTalents.Talents.AttackLv2);
                            talentTreeButons[1].objectTalents[1].button.gameObject.GetComponent<Image>().color = Color.red;
                            talentTreeButons[1].objectTalents[1].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                            FPSPlayerStats.TalentPoints -= 2;
                            UpdateTalentPointsText();
                        }
                        A2 = true;
                    }
                    break;
                case "Crafting2":
                    if (!C2)
                    {
                        if (FPSPlayerStats.TalentsUnlocked.AlreadyUnlockedTalent(playerTalents.Talents.CraftingLv2))
                        {
                            print("Ya lo tiene desbloqueado");
                        }
                        else if (FPSPlayerStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.CraftingLv2))
                        {
                            talentEvent.RaiseTalent(playerTalents.Talents.CraftingLv2);
                            talentTreeButons[2].objectTalents[1].button.gameObject.GetComponent<Image>().color = Color.red;
                            talentTreeButons[2].objectTalents[1].lineToUpperTalent.gameObject.GetComponent<Image>().color = Color.green;
                            FPSPlayerStats.TalentPoints -= 2;
                            UpdateTalentPointsText();
                        }
                        C2 = true;
                    }
                    break;
            }
        }
        if (FPSPlayerStats.TalentPoints >= 3)
        {
            switch (talent)
            {
                case "Health3":
                    if (!H3)
                    {
                        if (FPSPlayerStats.TalentsUnlocked.AlreadyUnlockedTalent(playerTalents.Talents.HealthLv3))
                        {
                            print("Ya lo tiene desbloqueado");
                        }
                        else if (FPSPlayerStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.HealthLv3))
                        {
                            talentEvent.RaiseTalent(playerTalents.Talents.HealthLv3);
                            talentTreeButons[0].objectTalents[2].button.gameObject.GetComponent<Image>().color = Color.red;
                            FPSPlayerStats.TalentPoints -= 3;
                            UpdateTalentPointsText();
                        }
                        H3 = true;
                    }
                    break;
                case "Attack3":
                    if (!A3)
                    {
                        if (FPSPlayerStats.TalentsUnlocked.AlreadyUnlockedTalent(playerTalents.Talents.AttackLv3))
                        {
                            print("Ya lo tiene desbloqueado");
                        }
                        else if (FPSPlayerStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.AttackLv3))
                        {
                            talentEvent.RaiseTalent(playerTalents.Talents.AttackLv3);
                            talentTreeButons[1].objectTalents[2].button.gameObject.GetComponent<Image>().color = Color.red;
                            FPSPlayerStats.TalentPoints -= 3;
                            UpdateTalentPointsText();
                        }
                        A3 = true;
                    }
                    break;
            }
        }

    }
    
    public void UpdateTalentPointsText()
    {
        talentPointsText.gameObject.GetComponent<Text>().text = "Points : " + FPSPlayerStats.TalentPoints;
    }
}
