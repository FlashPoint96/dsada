﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class playerTalents
{
    public enum Talents{
        None,HealthLv1, HealthLv2, HealthLv3,AttackLv1, AttackLv2, AttackLv3,CraftingLv1, CraftingLv2, CraftingLv3
    }
    private List<Talents> unlockedPlayerTalents;
    [SerializeField]
    public playerTalents()
    {
        unlockedPlayerTalents = new List<Talents>();
    }

    public List<Talents> getTalents()
    {
        return unlockedPlayerTalents;
    }

    public string getTalentName(Talents t)
    {
        switch (t)
        {
            case Talents.HealthLv1: return "Health1";
            case Talents.HealthLv2: return "Health2";
            case Talents.HealthLv3: return "Health3";
            case Talents.AttackLv1: return "Attack1";
            case Talents.AttackLv2: return "Attack2";
            case Talents.AttackLv3: return "Attack3";
            case Talents.CraftingLv1: return "Crafting1";
            case Talents.CraftingLv2: return "Crafting2";
            case Talents.CraftingLv3: return "Crafting3";
        }
        return "-1";
    }
    public bool UnlockTalent(Talents t)
    {
        Talents requirement = RequirementsSkill(t);
        if(requirement != Talents.None)
        {
            if (this.AlreadyUnlockedTalent(requirement))
            {
                unlockedPlayerTalents.Add(t);
                return true;
            }
            else
            {
                return false;
            }
        }else if (!AlreadyUnlockedTalent(t))
        {
            unlockedPlayerTalents.Add(t);
            return true;
        }
        return false;
    }

    public bool AlreadyUnlockedTalent(Talents t)
    {
        return unlockedPlayerTalents.Contains(t);
    }

    public Talents RequirementsSkill(Talents t)
    {
        switch (t)
        {
            case Talents.HealthLv2: return Talents.HealthLv1;
            case Talents.HealthLv3: return Talents.HealthLv2;
            case Talents.AttackLv2: return Talents.AttackLv1;
            case Talents.AttackLv3: return Talents.AttackLv2;
            case Talents.CraftingLv2: return Talents.CraftingLv1;
            case Talents.CraftingLv3: return Talents.CraftingLv2;
        }
        return Talents.None;
    }

      
}
