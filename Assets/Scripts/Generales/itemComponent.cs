﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemComponent : MonoBehaviour
{
    [SerializeField]
    private Item item;

    public Item getThisItem()
    {
        return item;
    }
}
