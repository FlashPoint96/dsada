﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class randomLightngLighter : MonoBehaviour
{
    private Light lighter;
    // Start is called before the first frame update
    void Start()
    {
        lighter = this.transform.GetChild(0).GetComponent<Light>();
        StartCoroutine(LightingRandom());
    }

    IEnumerator LightingRandom()
    {
        lighter.intensity = Random.Range(0, 2) + 1;
        lighter.range = Random.Range(1, 5) + 1;
        yield return new WaitForSeconds(5);
        StartCoroutine(LightingRandom());
    }
}
