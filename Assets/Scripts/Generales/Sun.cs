﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sun : MonoBehaviour
{
    public bool inDay;
    public GameObject point;
    private float time = 0;
    private float timeAction = 40;
    private bool done = false;

    Color darkerColor = new Color(0.18f, 0.21f, 0.22f);
    Color lighterColor = new Color(0.54f, 0.62f, 0.67f);

    void Update()
    {
        if (transform.position.y <= -30)
        {
            inDay = false;
            //RenderSettings.fog = true;
            transform.gameObject.GetComponent<Light>().intensity -= 0.006f;
            if (RenderSettings.fogColor.r < lighterColor.r) {
                print("enaadiecer "+ RenderSettings.fogColor);
                RenderSettings.fogColor *= 1.00015f; //subirlo sube velocidad
            }
        }
        else
        {
            inDay = true;
            //RenderSettings.fog = false;

            if (transform.gameObject.GetComponent<Light>().intensity < 0.9f) transform.gameObject.GetComponent<Light>().intensity += 0.006f;
            if (RenderSettings.fogColor.r > darkerColor.r) {
                print("enaaochecer "+ RenderSettings.fogColor);
                RenderSettings.fogColor *= 0.9999f; //bajarlo sube velocidad
            }
        }
        //RotateAround(punto sobre el que rotar, hacia que direccion, y aqui pone angulo pero yo lo uso para la velocidad de rotacion)
        this.transform.RotateAround(point.transform.position, this.transform.right, 0.8f * Time.deltaTime);
        FogMode();
    }
    private void FogMode()
    {
        time += Time.deltaTime;
        if(time >= timeAction)
        {
            time = 0;
            done = false;
            if (RenderSettings.fogDensity >= 0.09) StartCoroutine(Clear());
            else StartCoroutine(MoreFog());
        }
    }

    IEnumerator MoreFog()
    {
        print("FogMore " );

        float rand = UnityEngine.Random.Range(0.09f, 0.3f);

        while (!done)
        {
            RenderSettings.fogDensity += 0.0005f;
            if (RenderSettings.fogDensity >= rand) done = true;
            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator Clear()
    {
        print("FogClear " + RenderSettings.fogDensity);

        float rand = UnityEngine.Random.Range(0.001f, 0.02f);
        print("FogClear " + rand);

        while (!done)
        {
            print("FogClear " + RenderSettings.fogDensity);

            RenderSettings.fogDensity -= 0.00025f;
            if (RenderSettings.fogDensity <= rand) done = true;
            yield return new WaitForSeconds(0.1f);

        }
    }
}
