﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nav : MonoBehaviour
{
    public GameObject canvas;

    void Awake()
    {
        canvas.transform.GetChild(0).gameObject.SetActive(false);
    }
    void Start()
    {
        StartCoroutine(NavStart());
    }

    IEnumerator NavStart()
    {
        print("Nav me lanza");
        yield return new WaitForSeconds(5);
        canvas.transform.GetChild(0).gameObject.SetActive(true);
        print("Nav me enciende");
    }
}
