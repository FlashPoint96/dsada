﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radio : MonoBehaviour
{
    public AudioClip clip;

    void Start()
    {
        AudioSource.PlayClipAtPoint(clip, transform.position);
    }
}
