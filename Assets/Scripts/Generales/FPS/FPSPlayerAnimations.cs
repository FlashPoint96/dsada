﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSPlayerAnimations : MonoBehaviour
{
    private GameObject weapon;
    private AudioSource m_AudioSource;

    private void Start()
    {
        weapon = this.GetComponent<FPSPlayerWeaponController>().GetFirstActiveChild();
        m_AudioSource = GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {
        weapon = this.GetComponent<FPSPlayerWeaponController>().GetFirstActiveChild();
    }


    public void Walk(bool mvalue)
    {
        weapon.GetComponent<Animator>().SetBool("walk", mvalue);
    }
    public void Run(bool mvalue)
    {
        weapon.GetComponent<Animator>().SetBool("run", mvalue);
    }
    public void Recharge()
    {
        weapon.GetComponent<Animator>().SetTrigger("reload");
    }
    public void attack(string animacion)
    {
        switch (animacion)
        {
            case "BasicAttack":
                weapon.GetComponent<Animator>().SetTrigger("attack1");
                if(weapon.GetComponent<getWeaponSCO>().getWeapon().attackAudioClip == null){ }
                else
                {
                    m_AudioSource.clip = weapon.GetComponent<getWeaponSCO>().getWeapon().attackAudioClip;
                    m_AudioSource.Play();
                }
                //weapon.GetComponent<WeaponScriptableObject>().attackAudioClip;
                break;
            case "ComboAttack":
                weapon.GetComponent<Animator>().SetTrigger("attack2");
                if (weapon.GetComponent<getWeaponSCO>().getWeapon().attackAudioClip == null) { }
                else
                {
                    m_AudioSource.clip = weapon.GetComponent<getWeaponSCO>().getWeapon().attackAudioClip;
                    m_AudioSource.Play();
                }
                break;
        }
    }
    public void Aim(bool mvalue)
    {
        weapon.GetComponent<Animator>().SetBool("aim", mvalue);
    }

    public void show()
    {
        weapon.GetComponent<Animator>().SetTrigger("show");
    }

    public float GetClipLength(string anim)
    {

        RuntimeAnimatorController ac = weapon.GetComponent<Animator>().runtimeAnimatorController;
        float duracion = 0f;

        string animName = "A";
        switch (anim)
        {
            case "BasicAttack":
                animName = "Attack(1)";
                break;
            case "ComboAttack":
                animName = "Attack(2)";
                break;
            case "Idle":
                animName = "Idle";
                break;
            case "Run":
                animName = "Run";
                break;
            case "Walk":
                animName = "Walk";
                break;
        }

         for (int i = 0; i < ac.animationClips.Length; i++) {
            if (ac.animationClips[i].name == animName)
            {
                duracion = ac.animationClips[i].length;
            }
        }
        return duracion;
    }

    public bool CheckIfIsPlaying(string animationname)
    {
        if (weapon.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName(animationname))
        {
            return true;
        }
        return false;
    }


}
