﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSSwitchWeapon : MonoBehaviour
{
    public int m_actualWeapon;
    private AudioSource m_AudioSource;
    private GameObject actualWeaponGameObject;

    // Start is called before the first frame update
    void Start()
    {
        m_actualWeapon = this.GetComponent<FPSPlayerWeaponController>().actualWeapon;
        m_AudioSource = GetComponent<AudioSource>();
        actualWeaponGameObject = this.GetComponent<FPSPlayerWeaponController>().GetFirstActiveChild();
        m_actualWeapon = 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        /*if(Input.GetAxis("Mouse ScrollWheel") > 0f && !Input.GetMouseButton(1))
        {
            swapWeapon();
        }*/

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ActiveWeapon(0);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ActiveWeapon(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ActiveWeapon(2);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            ActiveWeapon(3);
        }
    }

    public void ActiveWeapon(int v)
    {
        if (this.transform.GetChild(0).childCount > v)
        {
                CheckIfIsLighterAndDesactive();
                CheckIfAimIsActivedAndDisableIt();
                this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(m_actualWeapon).gameObject.SetActive(false);
                this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(v).gameObject.SetActive(true);
                this.GetComponent<FPSPlayerWeaponController>().actualWeapon = m_actualWeapon;
                CheckIfIsLighterAndActive();
                m_actualWeapon = v;
                this.GetComponent<FPSPlayerWeaponController>().loadSCRValues();
        }
    }

    public void JustActive(int v)
    {
        if (this.transform.GetChild(0).childCount > v)
        {
            CheckIfIsLighterAndDesactive();
            CheckIfAimIsActivedAndDisableIt();
            this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(v).gameObject.SetActive(true);
            this.GetComponent<FPSPlayerWeaponController>().actualWeapon = m_actualWeapon;
            CheckIfIsLighterAndActive();
            m_actualWeapon = v;
            this.GetComponent<FPSPlayerWeaponController>().loadSCRValues();
        }
    }

    public void swapWeapon()
    {
        if (m_actualWeapon + 1 >= this.gameObject.transform.GetChild(0).childCount)
        {
            CheckIfIsLighterAndDesactive();
            CheckIfAimIsActivedAndDisableIt();
            this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(m_actualWeapon).gameObject.SetActive(false);
            this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.SetActive(true);
            m_actualWeapon = 0;
            this.GetComponent<FPSPlayerWeaponController>().actualWeapon = m_actualWeapon;
            CheckIfIsLighterAndActive();
        }
        else
        {
            CheckIfIsLighterAndDesactive();
            CheckIfAimIsActivedAndDisableIt();
            this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(m_actualWeapon).gameObject.SetActive(false);
            m_actualWeapon++;
            this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(m_actualWeapon).gameObject.SetActive(true);
            this.GetComponent<FPSPlayerWeaponController>().actualWeapon = m_actualWeapon;
            CheckIfIsLighterAndActive();
        }

    }

    private void CheckIfAimIsActivedAndDisableIt()
    {
        if (this.GetComponent<FPSPlayerWeaponController>().IsAiming())
        {
            this.GetComponent<FPSPlayerWeaponController>().setAiming(false);
            this.GetComponent<FPSPlayerAnimations>().Aim(false);
        }
    }

    public void CheckIfIsLighterAndDesactive()
    {
        if (this.GetComponent<FPSPlayerWeaponController>().GetFirstActiveChild().GetComponent<getWeaponSCO>().getWeapon().weaponID == 2)
        {
            this.GetComponent<FPSPlayerWeaponController>().GetFirstActiveChild().gameObject.transform.GetChild(0).gameObject.SetActive(false);
            this.GetComponent<FPSPlayerWeaponController>().isLighter = false;
            //StartCoroutine(EsperarSegundos(0f, false));
        }
    }
    public void CheckIfIsLighterAndActive()
    {
        if (this.GetComponent<FPSPlayerWeaponController>().GetFirstActiveChild().GetComponent<getWeaponSCO>().getWeapon().weaponID == 2)
        {
            m_AudioSource.clip = this.GetComponent<FPSPlayerWeaponController>().GetFirstActiveChild().GetComponent<getWeaponSCO>().getWeapon().attackAudioClip;
            m_AudioSource.PlayDelayed(0);
            if (this.GetComponent<FPSPlayerWeaponController>().GetFirstActiveChild().GetComponent<getWeaponSCO>().getWeapon().gas > 0)
            {
                StartCoroutine(EsperarSegundos(2.5f, true));
            }
        }
    }

    IEnumerator EsperarSegundos(float segundos,bool status)
    {
        yield return new WaitForSeconds(segundos);
        this.GetComponent<FPSPlayerWeaponController>().GetFirstActiveChild().gameObject.transform.GetChild(0).gameObject.SetActive(status);
        this.GetComponent<FPSPlayerWeaponController>().isLighter = status;
    }

}
