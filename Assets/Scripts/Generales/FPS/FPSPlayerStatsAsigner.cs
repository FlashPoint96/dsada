﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;
using TMPro;
public class FPSPlayerStatsAsigner : MonoBehaviour
{

    public GameEvent StaminaBar;
    public GameEvent pjgetHit;
    public GameEvent FoodBar;
    public GameEvent ThristBar;
    FirstPersonController controller;


    public FPSPlayerStats fps;
    private float ActualHP;
    [SerializeField]
    private bool Alive;
    [SerializeField]
    private float playerNoise;

    [SerializeField]
    private GameObject Hotbar;

    private float actualFood, actualWater, actualEnergy;

    private GameObject weapons;
    private Sun sun;
    public GameObject auMeHanDado;
    private bool firstStart = true;
    private bool safetyZone = false;
    private bool inCheckEvent = false;
    private bool inWaterEvent = false;
    private bool inEatEvent = false;
    private bool outWaterEvent = true;
    private bool outEatEvent = true;


    public Sprite defaultIMG;
    public GameObject sleepGameObject;
    // Start is called before the first frame update
    void Awake()
    {
        sleepGameObject = GameObject.FindGameObjectWithTag("sleepGameObject");
        sleepGameObject.SetActive(false);
        controller = GameObject.FindObjectOfType<FirstPersonController>();
        sun = FindObjectOfType<Sun>();
        weapons = this.transform.GetChild(0).gameObject;
        if (fps.Damage == 0)
        {
            AsignRandomValues();
        }
        Alive = true;
        actualEnergy = fps.ActualEnergy;
        actualFood = fps.ActualFood;
        actualWater = fps.ActualWater;
        ActualHP = fps.ActualHealth;

        ActualHP = Mathf.Clamp(ActualHP, 0, fps.MaxHealth);
        actualEnergy = Mathf.Clamp(actualEnergy, 0, fps.MaxEnergy);
        actualFood = Mathf.Clamp(actualFood, 0, fps.MaxFood);
        actualWater = Mathf.Clamp(actualWater, 0, fps.MaxWater);


        ActualHP = fps.ActualHealth;



    }
    private void Start()
    {
        updateStats();
        pjgetHit.Raise();   
                StaminaBar.Raise();
                FoodBar.Raise();
                ThristBar.Raise();

    }
    private void Update()
    {
        updateStats();
        pjgetHit.Raise();
        StaminaBar.Raise();
        FoodBar.Raise();
        ThristBar.Raise();
        if (isAlive())
        {
           StatsChanger();
        }
    }

    void updateStats()
    {

        ActualHP = Mathf.Clamp(ActualHP, 0, fps.MaxHealth);
        actualEnergy = Mathf.Clamp(actualEnergy, 0, fps.MaxEnergy);
        actualFood = Mathf.Clamp(actualFood, 0, fps.MaxFood);
        actualWater = Mathf.Clamp(actualWater, 0, fps.MaxWater);

        fps.ActualEnergy = actualEnergy;
        fps.ActualFood = actualFood;
        fps.ActualWater = actualWater;
        fps.ActualHealth = ActualHP;

    }
    private void FixedUpdate()
    {
        updateStats();
        if (ActualHP <= 0)
        {
            pjgetHit.Raise();

            Alive = false;
            PlayerIsDead();
        }
        if (firstStart)
        {
            loadWeapons();
            firstStart = false;
        }
        if(Hotbar != null)
        {
            loadHotBar();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            StartCoroutine(TryToSleep());
        }
        EnergyWaste();
        FoodWaste();
        WaterWaste();
    }

    IEnumerator TryToSleep()
    {
        if (getSecureZone())
        {
                sleepGameObject.SetActive(true);
                yield return new WaitForSeconds(4);
                Rest();
                sleepGameObject.transform.GetChild(0).gameObject.SetActive(true);
                yield return new WaitForSeconds(4);
                sleepGameObject.transform.GetChild(0).gameObject.SetActive(false);
                yield return new WaitForSeconds(4);
                sleepGameObject.SetActive(false);
                this.actualEnergy = this.fps.MaxEnergy - this.fps.EnergySpendPerSecond;
        }
        yield return new WaitForSeconds(0);
    }
    public bool getSecureZone()
    {
        Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, 10);
        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.gameObject.tag == "Enemy")
            {
                return false;
            }
        }
        return true;
    }

    private void StatsChanger()
    {
        if (this.actualEnergy > 1) inCheckEvent = false;
        if (this.actualEnergy <= 0)
        {
            StartCoroutine(imLowEnergy());
        }
        if (this.actualFood <= 0)
        {
            if (!inEatEvent)
            {
                inEatEvent = true;
                outEatEvent = false;
                this.fps.EnergySpendPerSecond += 0.02f;
            }
        }
        else
        {
            if (!outEatEvent)
            {
                inEatEvent = false;
                outEatEvent = true;
                this.fps.EnergySpendPerSecond -= 0.02f;
            }
        }
        if(this.actualWater <= 0)
        {
            if (!inWaterEvent)
            {
                inWaterEvent = true;
                outWaterEvent = false;
                this.fps.EnergySpendPerSecond += 0.02f;
            }
        }
        else
        {
            if (!outWaterEvent)
            {
                inWaterEvent = false;
                outWaterEvent = true;
                this.fps.EnergySpendPerSecond -= 0.02f;
            }
        }
    }
    IEnumerator imLowEnergy()
    {
        if (!inCheckEvent)
        {
            inCheckEvent = true;
            sleepGameObject.SetActive(true);
            yield return new WaitForSeconds(4);
            if (!safetyZone) checkEvent();
            else Rest();
            sleepGameObject.transform.GetChild(0).gameObject.SetActive(true);
            yield return new WaitForSeconds(4);
            sleepGameObject.transform.GetChild(0).gameObject.SetActive(false);
            yield return new WaitForSeconds(4);
            sleepGameObject.SetActive(false);

            this.actualEnergy = this.fps.MaxEnergy-this.fps.EnergySpendPerSecond;
            Time.timeScale = 1;
        }
    }

    private void checkEvent()
    {
        print("CHeckEvent");
        float rand = UnityEngine.Random.Range(1, 11);
        switch (EnemySpawner.instance.CheckWhichZoneName(this.transform.position))
        {
            case "ZoneLvL1":
                if (rand > 5) Rest();
                else if (rand <= 5 && rand > 2)
                {
                    ActualHP -= fps.MaxHealth * 0.1f;
                    sleepGameObject.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "Pierdes un 10% de tu vida";
                }
                else
                {
                    ActualHP -= fps.MaxHealth * 0.2f;
                    sleepGameObject.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "Pierdes un 20% de tu vida";
                }
                break;
            case "ZoneLvL2":
                if (rand > 9) Rest();
                else if (rand <= 9 && rand > 4)
                {
                    ActualHP -= fps.MaxHealth * 0.2f;
                    sleepGameObject.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "Pierdes un 20% de tu vida";
                }
                else
                {
                    ActualHP -= fps.MaxHealth * 0.3f;
                    sleepGameObject.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "Pierdes un 20% de tu vida";
                }
                break;
            case "ZoneLvL3":
                if (rand < 4) PlayerIsDead();
                else if (rand >= 4 && rand < 8)
                {
                    ActualHP -= fps.MaxHealth * 0.3f;
                    sleepGameObject.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "Pierdes un 30% de tu vida";
                }
                else
                {
                    ActualHP -= fps.MaxHealth * 0.5f;
                    sleepGameObject.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "Pierdes un 50% de tu vida";
                }
                break;
            case "ZoneLvL4":
                PlayerIsDead();
                break;
            case "ZoneLvL5":
                PlayerIsDead();
                break;
            case "ZoneLvL2B":
                if (rand > 9) Rest();
                else if (rand <= 9 && rand > 4)
                {
                    ActualHP -= fps.MaxHealth * 0.2f;
                    sleepGameObject.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "Pierdes un 20% de tu vida";
                }
                else
                {
                    ActualHP -= fps.MaxHealth * 0.3f;
                    sleepGameObject.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "Pierdes un 20% de tu vida";
                }
                break;
            case "ZoneLvL5B":
                PlayerIsDead();
                break;
            case "ZoneLvL4B":
                PlayerIsDead();
                break;
            default:
                sleepGameObject.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = EnemySpawner.instance.CheckWhichZoneName(this.transform.position);

                break;

        }
    }

    private void Dead()
    {
        ActualHP = 0;
        sleepGameObject.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "Tas Muerto";
    }

    private void Rest()
    {
        actualEnergy = fps.MaxEnergy;
        ActualHP += fps.MaxHealth * 0.1f;
        sleepGameObject.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "Descansas sin mayor problema";
    }
    private void WaterWaste()
    {
       actualWater -= fps.WaterSpendPerSecond;
    }

    private void FoodWaste()
    {
        actualFood-= fps.FoodSpendPerSecond;
    }

    private void EnergyWaste()
    {
        actualEnergy -= fps.EnergySpendPerSecond;
    }

   /* public void loadHotBar()
    {
        List<int> auxVariables = new List<int>();
        for (int i = 0; i < HotbarGameObjects.transform.childCount; i++)
        {
            if(transform.GetChild(0).GetChild(i) == null)
            {

            }
            else
            {
                HotbarGameObjects.transform.GetChild(i).GetChild(1).GetComponent<Image>().sprite = this.transform.GetChild(0).GetChild(i).GetComponent<getWeaponSCO>().getWeapon().icon;
                auxVariables.Add(i);
                int aux = i + 1;
                HotbarGameObjects.transform.GetChild(i).GetChild(2).GetComponent<Text>().text = "" + aux;
                HotbarGameObjects.transform.GetChild(i).GetChild(3).GetComponent<Text>().text = "";
                HotbarGameObjects.transform.GetChild(i).GetChild(4).GetComponent<ButtonOnClickHotBar>().setHotbarSlot(weapons.gameObject.transform.GetChild(i).gameObject.GetComponent<getWeaponSCO>().getWeapon(), i, HotbarGameObjects, this);
            }
        }

    }
   */
    public void loadHotBar()
    {
        List<int> auxVariables = new List<int>();
        for(int i = 0; i < Hotbar.transform.childCount; i++)
        {
            Hotbar.transform.GetChild(i).GetChild(1).GetComponent<Image>().sprite = defaultIMG;
            Hotbar.transform.GetChild(i).GetChild(2).GetComponent<Text>().text = "";
            Hotbar.transform.GetChild(i).GetChild(3).GetComponent<Text>().text = "";
        }


        for (int i = 0; i < this.transform.GetChild(0).childCount; i++)
        {
            Hotbar.transform.GetChild(i).GetChild(1).GetComponent<Image>().sprite = this.transform.GetChild(0).GetChild(i).GetComponent<getWeaponSCO>().getWeapon().icon;
            auxVariables.Add(i);
            int aux = i + 1;
            Hotbar.transform.GetChild(i).GetChild(2).GetComponent<Text>().text = "" + aux;
            Hotbar.transform.GetChild(i).GetChild(3).GetComponent<Text>().text = "";
            Hotbar.transform.GetChild(i).GetChild(4).GetComponent<ButtonOnClickHotBar>().setHotbarSlot(weapons.gameObject.transform.GetChild(i).gameObject.GetComponent<getWeaponSCO>().getWeapon(), i, Hotbar, this);
        }

    }


    private void loadWeapons()
    {
        foreach (GameObject item in fps.GameObjectsweapons)
        {
            GameObject newWeapon = Instantiate(item, new Vector3(0, 0, 0), Quaternion.identity);
            newWeapon.transform.SetParent(this.transform.GetChild(0), false);
            newWeapon.transform.localPosition = new Vector3(0, -1.7f, 0);
            newWeapon.transform.localScale = Vector3.one;
            newWeapon.SetActive(false);
        }   
    }

    public void RemoveWeapon(GameObject weapon)
    {
        print("CONTAINS" + weapon);
        GameObject aux = null;
        foreach (GameObject go in fps.GameObjectsweapons)
        {
            if(go.GetComponent<getWeaponSCO>().getWeapon().weaponID == weapon.GetComponent<getWeaponSCO>().getWeapon().weaponID)
            {
                aux = go;
            }
        }
        fps.GameObjectsweapons.Remove(aux);
        foreach (Transform child in transform.GetChild(0).transform)
        {
            if(child.GetComponent<getWeaponSCO>().getWeapon().weaponID == weapon.GetComponent<getWeaponSCO>().getWeapon().weaponID)
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        this.loadHotBar();

    }

    private void PlayerIsDead()
    {
        print("IS DEAD");
        this.fps.Damage = 0;
        //this.AsignRandomValues();
        this.GetComponentInChildren<InventoryController>().inventario.Slot.Clear();
        StartCoroutine(DeadLoadScene());
    }

    IEnumerator DeadLoadScene()
    {
        sleepGameObject.SetActive(true);
        yield return new WaitForSeconds(4);
        sleepGameObject.transform.GetChild(0).gameObject.SetActive(true);
        sleepGameObject.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "R.I.P.";
        yield return new WaitForSeconds(7);
        sleepGameObject.transform.GetChild(0).gameObject.SetActive(false);
        SceneManager.LoadScene("Menu");
        yield return new WaitForSeconds(5);
        GameObject.Destroy(this.gameObject);
    }

    public void AsignRandomValues()
    {
        fps.Damage = UnityEngine.Random.Range(25,50);
        fps.MaxHealth = UnityEngine.Random.Range(300, 500);
        fps.Defense = UnityEngine.Random.Range(30, 50);
        fps.CDPorce = UnityEngine.Random.Range(3, 8);
        fps.MaxWater = 100;
        fps.MaxFood = 100;
        fps.MaxEnergy = 100;
        fps.EnergySpendPerSecond = 0.001f;
        fps.FoodSpendPerSecond = 0.004f;
        fps.WaterSpendPerSecond = 0.006f;
        fps.ActualEnergy = fps.MaxEnergy;
        fps.ActualHealth = fps.MaxHealth;
        fps.ActualFood = fps.MaxFood;
        fps.ActualWater = fps.MaxWater;
        fps.TalentsUnlocked = new playerTalents();
        fps.GameObjectsweapons = new List<GameObject>();
        this.ActualHP = fps.ActualHealth;
        print(fps.TalentsUnlocked);
    }

    public void SetNoise(float noise)
    {
        if(sun.inDay)
        {
            playerNoise = noise;
        }
        else
        {
            playerNoise = noise * 2;
        }
    }
    public float GetNoise()
    {
        return playerNoise;
    }
    public FPSPlayerStats getPlayerStats()
    {
        return this.fps;
    }

    public bool isAlive()
    {
        return Alive;
    }

    public void damageThis(float damage)
    {

        StartCoroutine(AUMEHANDADO());
        this.ActualHP -= (damage-fps.Defense);
        fps.ActualHealth = this.ActualHP;
        pjgetHit.Raise();

    }

    public void healMe(int health)
    {
        pjgetHit.Raise();

        this.ActualHP += health;
        if (this.ActualHP >= this.fps.MaxHealth)
        {
            this.ActualHP = this.fps.MaxHealth;
        }
        else
        {
            fps.ActualHealth = this.ActualHP;
        }
    }
    public void WaterMe(int health)
    {
        pjgetHit.Raise();

        this.actualWater += health;
        if (this.actualWater >= 100)
        {
            this.actualWater = 100;
        }
    }

    public void FoodMe(int health)
    {
        pjgetHit.Raise();

        this.actualFood += health;
        if (this.actualFood >= 100)
        {
            this.actualFood = 100;
        }
    }
    public float getThisHP()
    {
        return this.ActualHP;
    }

    IEnumerator AUMEHANDADO()
    {

        auMeHanDado.SetActive(true);
        yield return new WaitForSeconds(1f);
        auMeHanDado.SetActive(false);

    }
    public bool IsAgentOnNavMesh(GameObject agentObject)
    {
        Vector3 agentPosition = agentObject.transform.position;
        NavMeshHit hit;

        // Check for nearest point on navmesh to agent, within onMeshThreshold
        if (NavMesh.SamplePosition(agentPosition, out hit, 3, NavMesh.AllAreas))
        {
            // Check if the positions are vertically aligned
            if (Mathf.Approximately(agentPosition.x, hit.position.x)
                && Mathf.Approximately(agentPosition.z, hit.position.z))
            {
                // Lastly, check if object is below navmesh
                return agentPosition.y >= hit.position.y;
            }
        }

        return false;
    }

    internal void RemoveWeapon()
    {
        throw new NotImplementedException();
    }
}
