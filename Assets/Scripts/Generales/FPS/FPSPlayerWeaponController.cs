﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSPlayerWeaponController : MonoBehaviour
{

    public GameEvent fuelBar;

    private Camera camara;
    private FPSPlayerAnimations FPSa;
    private WeaponScriptableObject WSO;
    public GameObject grenade;
    public int actualWeapon = 0;
    public bool isLighter = false;
    private bool aim = false;
    private bool weaponReload = false;
    private float CDFirstAttack;
    private float CDComboAttack;
    private float timeBetweenShots;
    private float grenadeThrowForce = 1100;
    private float grenadeThrowVelocity = 9;


    // Start is called before the first frame update
    void Start()
    {
        fuelBar.Raise();
        camara = this.transform.GetChild(0).GetComponent<Camera>();
        FPSa = this.GetComponent<FPSPlayerAnimations>();
        loadSCRValues();
    }

    public void loadSCRValues()
    {
        WSO = this.GetFirstActiveChild().GetComponent<getWeaponSCO>().getWeapon();
        CDFirstAttack = WSO.timeBetweenShots;
        CDComboAttack = WSO.CDcomboAttack;
        timeBetweenShots = WSO.timeBetweenShots;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!this.GetComponentInChildren<PauseController>().pauseActive && !this.GetComponentInChildren<InventoryController>().inventoryActive)
        {
            CDRecharging();
            if (FPSa.CheckIfIsPlaying("reload"))
            {
                weaponReload = true;
            }
            else
            {
                weaponReload = false;
            }

            if (isLighter)
            {
                if (this.GetFirstActiveChild().GetComponent<getWeaponSCO>().getWeapon().gas > 0)
                {
                    this.GetFirstActiveChild().GetComponent<getWeaponSCO>().getWeapon().gas -= Time.deltaTime;
                    fuelBar.Raise();

                }
                else
                {
                    this.GetFirstActiveChild().GetComponent<getWeaponSCO>().getWeapon().gas = 0;
                    this.GetComponent<FPSSwitchWeapon>().CheckIfIsLighterAndDesactive();
                    fuelBar.Raise();
                }
            }
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                WSO = this.GetFirstActiveChild().GetComponent<getWeaponSCO>().getWeapon();
                if (CDFirstAttack <= 0 && !WSO.createsLight && !weaponReload)
                {
                    if (aim)
                    {
                        //El ataque combo en verdad no existe para las armas el comboattack se usa para cuando esta disparando mientras esta apuntando.
                        FPSa.attack("ComboAttack");
                    }
                    else
                    {
                        //Si la arma es a distancia
                        if (WSO.isRanged)
                        {
                            if (WSO.weaponID == 101)
                            {
                                GrenadeThrow();
                                weaponReload = true;
                                StartCoroutine(WaitGrenade());
                                FPSa.attack("BasicAttack");
                            }
                            else
                            {
                                if (WSO.isSemiAuto)
                                {
                                    StartCoroutine(ShotForSemi());
                                }
                                else
                                {
                                    ShotRayCast();
                                }
                            }
                        }
                        else
                        {
                            FPSa.attack("BasicAttack");
                            StartCoroutine(AttackMelee("BasicAttack"));
                        }
                    }
                    CDFirstAttack = WSO.timeBetweenShots;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                WSO = this.GetFirstActiveChild().GetComponent<getWeaponSCO>().getWeapon();
                if (WSO.haveCombo && CDComboAttack <= 0 && !WSO.createsLight && !weaponReload)
                {
                    FPSa.attack("ComboAttack");
                    StartCoroutine(AttackMelee("ComboAttack"));
                    CDComboAttack = WSO.CDcomboAttack;
                }
                else if (WSO.isRanged && !WSO.createsLight && aim && !weaponReload)
                {
                    FPSa.Aim(false);
                    aim = false;
                }
                else if (WSO.isRanged && !WSO.createsLight && !aim && !weaponReload)
                {
                    FPSa.Aim(true);
                    aim = true;
                }
            }

            if (WSO.createsLight)
            {
                //systema particulas y luz
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                WSO = this.GetFirstActiveChild().GetComponent<getWeaponSCO>().getWeapon();
                startReload();
            }
        }
    }
    IEnumerator WaitGrenade()
    {
        yield return new WaitForSeconds(2);
    }
    private void startReload()
    {
        if (this.WSO.isRanged)
        {
            if(WSO.maxShotsTotal != 0)
            {
                if (WSO.actualShotsInCharger != WSO.maxShotsCharger)
                {
                    int amountBullets = WSO.maxShotsCharger - WSO.actualShotsInCharger;
                    if (WSO.maxShotsTotal >= amountBullets)
                    {
                        WSO.actualShotsInCharger = WSO.maxShotsCharger;
                        WSO.maxShotsTotal -= amountBullets;
                        if (WSO.reloadClip != null)
                        {
                            AudioSource.PlayClipAtPoint(WSO.reloadClip, this.transform.position);
                        }
                        FPSa.Recharge();
                    }
                    else
                    {
                        WSO.actualShotsInCharger += WSO.maxShotsTotal;
                        WSO.maxShotsTotal = 0;
                        if (WSO.reloadClip != null)
                        {
                            AudioSource.PlayClipAtPoint(WSO.reloadClip, this.transform.position);
                        }
                        FPSa.Recharge();
                    }
                }
            }
        }
    }

    void ShotRayCast()
    {
        if (WSO.actualShotsInCharger > 0)
        {
            timeBetweenShots = WSO.timeBetweenShots;
            ShotNow(false);
        }
    }
    IEnumerator ShotForSemi()
    {
        for (int i = 0; i < WSO.numberOfShosOnSemi; i++)
        {
            if (WSO.actualShotsInCharger > 0)
            {
                ShotNow(true);
                yield return new WaitForSeconds(0.3f);
            }
        }
    }

    public void ShotNow(bool disp)
    {
        if (disp)
        {
            RaycastHit hit;
            float damage = 0;
            WSO.actualShotsInCharger--;
            damage += this.GetComponent<FPSPlayerStatsAsigner>().getPlayerStats().Damage;
            damage += this.WSO.damage;
            Vector3 noRecto = new Vector3(camara.transform.forward.x + UnityEngine.Random.Range(-WSO.dispersion, +WSO.dispersion), camara.transform.forward.y + UnityEngine.Random.Range(-WSO.dispersion, +WSO.dispersion), camara.transform.forward.z + UnityEngine.Random.Range(-WSO.dispersion, +WSO.dispersion));
            if (Physics.Raycast(camara.transform.position, noRecto, out hit,WSO.weaponRange)){
                Debug.DrawRay(camara.transform.position, noRecto * WSO.weaponRange, Color.yellow, 30f);
                if (hit.collider.tag == "Enemy")
                {
                    this.transform.gameObject.GetComponent<InteractionController>().setEnemyTarget(hit.collider.gameObject.GetComponent<IAStats>());
                    hit.collider.gameObject.GetComponent<IAStats>().damageThis(damage, this.gameObject);
                }
                else if (hit.collider.gameObject.name == "Head")
                {
                    hit.collider.gameObject.GetComponent<IAStats>().damageThis(damage * 2, this.gameObject);
                    this.transform.gameObject.GetComponent<InteractionController>().setEnemyTarget(hit.collider.gameObject.GetComponent<IAStats>());
                }
            }

        }
        else
        {
            float damage = 0;
            WSO.actualShotsInCharger--;
            damage += this.GetComponent<FPSPlayerStatsAsigner>().getPlayerStats().Damage;
            damage += this.WSO.damage;
            RaycastHit[] hits;
            hits = Physics.RaycastAll(camara.transform.position, camara.transform.forward, WSO.weaponRange);
            for (int i = 0; i < hits.Length; i++)
            {
                    FPSa.attack("BasicAttack");
                    this.GetComponent<FPSPlayerStatsAsigner>().SetNoise(40);
                    RaycastHit hit = hits[i];
                    StartCoroutine(CreateVFX());
                    //Physics.IgnoreCollision(hit.collider, GetComponent<CapsuleCollider>());
                    Debug.DrawRay(camara.transform.position, camara.transform.forward, Color.yellow, 30f);
                    print(hit.collider.name);
                    if (hit.collider.name == "Damage")
                    {
                        hit.collider.GetComponentInParent<IAStats>().damageThis(damage, this.gameObject);
                        this.transform.gameObject.GetComponent<InteractionController>().setEnemyTarget(hit.collider.gameObject.GetComponentInParent<IAStats>());
                    }
                    if (hit.collider.tag == "Enemy")
                    {
                        hit.collider.gameObject.GetComponent<IAStats>().damageThis(damage, this.gameObject);
                        this.transform.gameObject.GetComponent<InteractionController>().setEnemyTarget(hit.collider.gameObject.GetComponent<IAStats>());

                    }
                    else if (hit.collider.gameObject.name == "Head")
                  {
                        this.transform.gameObject.GetComponent<InteractionController>().setEnemyTarget(hit.collider.gameObject.GetComponentInParent<IAStats>());
                        hit.collider.gameObject.GetComponentInParent<IAStats>().damageThis(damage * 2, this.gameObject);
                }
            }
        }
    }

    IEnumerator CreateVFX()
    {
        this.GetFirstActiveChild().transform.GetChild(0).gameObject.SetActive(false);
        this.GetFirstActiveChild().transform.GetChild(0).gameObject.SetActive(true);
        //this.GetFirstActiveChild().transform.GetChild(0).GetComponent<ParticleSystem>().Play();
        //this.GetFirstActiveChild().transform.GetChild(0).GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(this.GetFirstActiveChild().transform.GetChild(0).GetComponent<AudioSource>().clip.length);
       // this.GetFirstActiveChild().transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
        this.GetFirstActiveChild().transform.GetChild(0).gameObject.SetActive(false);
    }

    void GrenadeThrow()
    {
        GameObject auxBomb = Instantiate(grenade, camara.transform.position, Quaternion.identity);
        auxBomb.GetComponent<Rigidbody>().AddForce(transform.TransformDirection(Vector3.forward) * grenadeThrowForce);
        auxBomb.GetComponent<Rigidbody>().velocity = GetComponentInChildren<Camera>().transform.forward * grenadeThrowVelocity;
        GrenadeThrowedChangeWeapon();

    }

    private void GrenadeThrowedChangeWeapon()
    {
        GameObject aux = this.GetFirstActiveChild();
        if(aux.GetComponent<getWeaponSCO>().getWeapon().weaponID == 101)
        {
            this.GetComponent<FPSPlayerStatsAsigner>().RemoveWeapon(this.GetFirstActiveChild());
            this.GetComponent<FPSSwitchWeapon>().JustActive(0);
            this.GetComponent<FPSPlayerStatsAsigner>().loadHotBar();
        }
    }

    IEnumerator AttackMelee(string attackname)
    {
        if(attackname == "ComboAttack")
        {
            this.GetComponentInChildren<DamageEnemy>().setAsComboAttack();
        }
        this.transform.GetChild(0).GetComponent<SphereCollider>().enabled = true;
        yield return new WaitForSeconds(FPSa.GetClipLength(attackname)/2);
        this.transform.GetChild(0).GetComponent<SphereCollider>().enabled = false;
        this.GetComponentInChildren<DamageEnemy>().setAsNormalAttack();
    }

    private void CDRecharging()
    {
        if (CDFirstAttack > 0)
        {
            CDFirstAttack -= Time.deltaTime;
        }
        if(CDComboAttack>0)
        {
            CDComboAttack -= Time.deltaTime;
        }
    }

    public GameObject GetFirstActiveChild()
    {
        for (int i = 0; i < this.transform.GetChild(0).gameObject.transform.childCount; i++)
        {
            if (this.transform.GetChild(0).gameObject.transform.GetChild(i).gameObject.activeSelf == true)
            {
                return this.transform.GetChild(0).gameObject.transform.GetChild(i).gameObject;
            }
        }
        return null;
    }

    public bool IsAiming()
    {
        return aim;
    }
    public void setAiming(bool mvalue)
    {
        aim = mvalue;
    }
}
