﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSTalentTreeManager : MonoBehaviour
{
    private FPSPlayerStats FPStats;

    private void Awake()
    {
        FPStats = this.GetComponent<FPSPlayerStatsAsigner>().fps;
    }
    public void NewTalentUnlocked(playerTalents.Talents t)
    {
        switch (t)
        {
            case playerTalents.Talents.HealthLv1:
                print("HAS DESBLOQUEADO EL TALENTO DE " + playerTalents.Talents.HealthLv1);
                FPStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.HealthLv1);
                float aux = FPStats.MaxHealth * 0.2f;
                FPStats.MaxHealth += aux;
            break;
            case playerTalents.Talents.HealthLv2:
                print("HAS DESBLOQUEADO EL TALENTO DE " + playerTalents.Talents.HealthLv2);
                FPStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.HealthLv2);
                float aux1 = FPStats.Defense * 0.25f;
                FPStats.Defense += aux1;
                FPStats.MaxHealth += 50;
            break;
            case playerTalents.Talents.HealthLv3:
                print("HAS DESBLOQUEADO EL TALENTO DE " + playerTalents.Talents.HealthLv3);
                FPStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.HealthLv3);
                float aux2 = FPStats.MaxEnergy * 0.5f;
                FPStats.MaxEnergy += aux2;
                FPStats.EnergySpendPerSecond = 0.000001f;
                break;
            case playerTalents.Talents.AttackLv1:
                print("HAS DESBLOQUEADO EL TALENTO DE " + playerTalents.Talents.AttackLv1);
                FPStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.AttackLv1);
                float aux3 = FPStats.Damage * 0.30f;
                FPStats.Damage += aux3;
                break;
            case playerTalents.Talents.AttackLv2:
                print("HAS DESBLOQUEADO EL TALENTO DE " + playerTalents.Talents.AttackLv2);
                FPStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.AttackLv2);
                FPStats.CDPorce += 25;
                FPStats.Damage += 50;
                break;
            case playerTalents.Talents.AttackLv3:
                print("HAS DESBLOQUEADO EL TALENTO DE " + playerTalents.Talents.AttackLv3);
                FPStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.AttackLv3);
                float aux4 = FPStats.Damage * 0.25f;
                FPStats.CDPorce += 30;
                FPStats.Damage += aux4;
                break;
            case playerTalents.Talents.CraftingLv1:
                print("HAS DESBLOQUEADO EL TALENTO DE " + playerTalents.Talents.CraftingLv1);
                FPStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.CraftingLv1);
                foreach(CraftingRecipe CR in this.GetComponent<CraftingSystem>().getRecipes())
                {
                    if(CR.getItemCraft().id == 103)
                    {
                        CR.isCrafteable = true;
                    }
                }
                break;
            case playerTalents.Talents.CraftingLv2:
                print("HAS DESBLOQUEADO EL TALENTO DE " + playerTalents.Talents.CraftingLv2);
                FPStats.TalentsUnlocked.UnlockTalent(playerTalents.Talents.CraftingLv2);
                foreach (CraftingRecipe CR in this.GetComponent<CraftingSystem>().getRecipes())
                {
                    if (CR.getItemCraft().Name.Equals("Automatic Rifle"))
                    {
                        CR.isCrafteable = true;
                    }
                }
                break;
        }
    }
}
