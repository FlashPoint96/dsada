﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FPSPlayerStats", order = 1)]
public class FPSPlayerStats : ScriptableObject
{
    public float TalentPoints;
    public float Damage;
    public float MaxHealth;
    public float ActualHealth;
    public float Defense;
    public float CDPorce;
    public float MaxFood;
    public float ActualFood;
    public float ActualEnergy;
    public float ActualWater;
    public float MaxWater;
    public float MaxEnergy;
    public float EnergySpendPerSecond;
    public float FoodSpendPerSecond;
    public float WaterSpendPerSecond;
    public playerTalents TalentsUnlocked;
    public List<GameObject> GameObjectsweapons;



}
