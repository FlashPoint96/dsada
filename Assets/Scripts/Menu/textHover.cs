﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class textHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler 
{

    public TextMeshProUGUI txt;
  

    public void OnPointerEnter(PointerEventData eventData)
    {
        txt.color = Color.red; //Or however you do your color
        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        txt.color = Color.white; //Or however you do your color

    }



}
