﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuCrosshairController : MonoBehaviour
{
    public CsScriptableObject CrosshairSettings;

    //Variables que guardan los datos
    public static float altura;
    public static float anchura;
    public static Color color;
    public static bool dinamico;

    //Variables que recogen los datos
    public Slider height;
    public Slider width;
    public Toggle dinamToggle;
    public Dropdown colorSelector;

    //Variables Default
    float DefAlt;
    float DefAnch;
    Color DefColor;
    bool DefDin;

    //Imagen de numeros de slider
    public Text AlturaTxt;
    public Text AnchuraTxt;

    public static bool ok = false;

    // Start is called before the first frame update
    void Start()
    {
        ok = false;
        //Inicializamos las variables
        altura = this.CrosshairSettings.Altura;
        anchura = this.CrosshairSettings.Anchura;
        color = this.CrosshairSettings.color;
        dinamico = this.CrosshairSettings.dinamico;

        //Default Values 
        DefAlt = this.CrosshairSettings.Altura;
        DefAnch = this.CrosshairSettings.Anchura;
        DefColor = this.CrosshairSettings.color;
        DefDin = this.CrosshairSettings.dinamico;

        //Valores recogidos 
        this.height.value = altura;
        this.width.value = anchura;
        this.dinamToggle.isOn = dinamico;

    }

    // Update is called once per frame
    void Update()
    {
        AlturaTxt.text = "" + altura;
        AnchuraTxt.text = "" + anchura;


        print(altura + " Altura");
        print(anchura + " anchura");
        print(DefColor + " color");
        print(dinamico + " dinamico");


        altura = this.height.value;
        anchura = this.width.value;
        switch (this.colorSelector.value)
        {
            //gris
            case 0:
                color = Color.gray;
                break;
            //Blanco
            case 1:
                color = Color.white;
                break;
            //Rojo
            case 2:
                color = Color.red;
                break;
            //Verde
            case 3:
                color = Color.green;
                break;
            //Azul
            case 4:
                color = Color.blue;
                break;
            //Amarillo
            case 5:
                color = Color.yellow;
                break;
        }
        dinamico = this.dinamToggle.isOn;
    }

    //Funcion onClick Ok button del menu Crosshair
    public void OkCrosshair()
    {
        //Guarda los datos 
        altura = this.height.value;
        anchura = this.width.value;
        switch (this.colorSelector.value)
        {
            //gris
            case 0:
                color = Color.gray;
                this.SetString("color", Color.gray.ToString());
                break;
            //Blanco
            case 1:
                color = Color.white;
                this.SetString("color", Color.white.ToString());
                break;
            //Rojo
            case 2:
                color = Color.red; 
                this.SetString("color", Color.red.ToString());
                break;
            //Verde
            case 3:
                color = Color.green;
                this.SetString("color", Color.green.ToString());
                break;
            //Azul
            case 4:
                color = Color.blue;
                this.SetString("color", Color.blue.ToString());
                break;
            //Amarillo
            case 5:
                color = Color.yellow;
                this.SetString("color", Color.yellow.ToString());
                break;
        }
        dinamico = this.dinamToggle.isOn;
        if (dinamico)
        {
            this.SetFloat("dinamico", 1);
        }
        else
        {
            this.SetFloat("dinamico", 0);
        }
        this.SetFloat("altura", altura);
        this.SetFloat("anchura", anchura);



    }
    public void playCrosshair()
    {




    }



    //Funcion OnClick BackButton menu Crosshair
    public void BackCrosshair()
    {
        //Deja los datos anteriores 
        altura = DefAlt;
        anchura = DefAnch;
        color = DefColor;
        dinamico = DefDin;

    }

    public void setTrueOk()
    {
        ok = true;
    }

    public void SetString(string KeyName, string Value)
    {
        PlayerPrefs.SetString(KeyName, Value);
    }
    public void SetFloat(string KeyName, float Value)
    {
        PlayerPrefs.SetFloat(KeyName, Value);
    }
}
