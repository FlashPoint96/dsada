﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class PauseController : MonoBehaviour
{
    public bool pauseActive = false;
    private InventoryController inventory;
    private GameObject hotbar;
    private GameObject HUD;
    private GameObject minimap;
    private AudioSource[] allAudioSources;
    // Update is called once per frame

    void Start()
    {
  
        inventory = GameObject.FindGameObjectWithTag("manager").GetComponent<InventoryController>();
        hotbar = GameObject.FindGameObjectWithTag("hotbar");
        HUD = GameObject.Find("HUDpj");
        minimap = GameObject.Find("[HUD Navigation Canvas]");
    }

    void Update()
    {
        //MouseLook
        if (Input.GetKeyDown(KeyCode.P) && pauseActive==false && inventory.inventoryActive==false)
        {

            pauseActive = true;
            stopAllAudio();
        }
        else if (Input.GetKeyDown(KeyCode.P) && pauseActive)
        {

            pauseActive = false;
            Time.timeScale = 1;
            resumeAllAudio();
        }
        if (pauseActive)
        {
            HUD.SetActive(false);
            minimap.SetActive(false);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            hotbar.gameObject.SetActive(false);
            
        }
        else if (pauseActive || inventory.inventoryActive)
        {
            HUD.SetActive(false);
            minimap.SetActive(false);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

        }
        else
        {
            HUD.SetActive(true);
            minimap.SetActive(true);
            //resumeAllAudio();
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            hotbar.gameObject.SetActive(true);
        }

        if (pauseActive)
        {

            Time.timeScale = 0;
        }
        
    }

    public void ExitButton()
    {
        Application.Quit();
        Time.timeScale = 1;
        //SceneManager.LoadScene(0);
        
    }
    public void ResumeButton()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1;
        pauseActive = false;
        resumeAllAudio();
    }

    public void stopAllAudio()
    {
        allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        foreach (AudioSource audioS in allAudioSources)
        {
            audioS.Pause();


        }
    }
    public void resumeAllAudio()
    {
        allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        foreach (AudioSource audioS in allAudioSources)
        {
            audioS.UnPause();


        }
    }
    
}
