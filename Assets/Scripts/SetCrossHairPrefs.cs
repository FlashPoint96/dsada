﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCrossHairPrefs : MonoBehaviour
{
    [SerializeField]
    private CsScriptableObject csSettings;
    // Start is called before the first frame update
    void Start()
    {
        loadSettings();
    }

    private void loadSettings()
    {
        if(GetFloat("altura") != null)
        {
            csSettings.Altura = GetFloat("altura");
        }
        if (GetFloat("anchura") != null)
        {
            csSettings.Anchura = GetFloat("anchura");
        }
        if (GetFloat("dinamico") == 1f)
        {
            csSettings.dinamico = true;
        }
        else
        {
            csSettings.dinamico = false;
        }
        if(getColor("color") != null)
        {
            csSettings.color = getColor("color");
        }

    }
    public float GetFloat(string KeyName)
    {
        return PlayerPrefs.GetFloat(KeyName);
    }

    public Color getColor(string KeyName)
    {
        string auxColor = PlayerPrefs.GetString(KeyName);
        auxColor = auxColor.Replace("RGBA(", "");
        auxColor = auxColor.Replace(")", "");
        var strings = auxColor.Split(","[0]);
        Color outputColor = new Color();
        for (var i = 0; i < 4; i++)
        {
            outputColor[i] = System.Single.Parse(strings[i]);
        }
        return outputColor;
    }
}
