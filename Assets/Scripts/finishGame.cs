﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class finishGame : MonoBehaviour
{
   /* private void OnCollisionEnter(Collision collision)
    {
        print("FINISHED GAME "+collision.gameObject.tag);
        if(collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<FPSPlayerStatsAsigner>().fps.Damage = 0;
            collision.gameObject.GetComponentInChildren<InventoryController>().inventario.Slot.Clear();
            SceneManager.LoadScene("WinnerScene");
        }
    }*/
    private void OnTriggerEnter(Collider other)
    {
        print("FINISHED GAME " + other.gameObject.tag);
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<FPSPlayerStatsAsigner>().fps.Damage = 0;
            other.gameObject.GetComponentInChildren<InventoryController>().inventario.Slot.Clear();
            other.gameObject.GetComponent<FPSPlayerStatsAsigner>().AsignRandomValues();
            SceneManager.LoadScene("WinnerScene");
        }
    }
}
