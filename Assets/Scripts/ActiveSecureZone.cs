﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveSecureZone : MonoBehaviour
{

    [SerializeField]
    private string zoneName;

    public string getZoneName()
    {
        Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, 10);
        foreach (var hitCollider in hitColliders)
        {
            if(hitCollider.gameObject.tag == "Enemy")
            {
                print("EnemigosdCerca");
                zoneName = "EnemigosCerca";
            }
        }
        return zoneName;
    }



    public void changeLightToActive()
    {
        this.GetComponent<Light>().intensity = 5;
        this.GetComponent<Light>().color = Color.red;
    }
}
