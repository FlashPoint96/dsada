﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class amountItemsPerObjectSpawn
{
    public GameObject item;
    public int amount;
}
public class itemSpawner : MonoBehaviour
{
    [SerializeField]
    private List<amountItemsPerObjectSpawn> itemsGameObjects = new List<amountItemsPerObjectSpawn>();

    private BoxCollider bc;
    private GameObject player;
    private Vector3 posPlayer;
    private Vector3 radius;
    private float dist = 100;
    private Vector3 dir = new Vector3(0, -1, 0);
    private Vector3 randomPosItem;
    private void Start()
    {
        load();
    }

    private void load()
    {
        bc = this.GetComponent<BoxCollider>();
        player = GameObject.Find("FPSController").gameObject;
        posPlayer = player.transform.position;
        radius = new Vector3(100, 100, 100);
        bc.size = radius;
        bc.isTrigger = true;
        posPlayer = player.transform.position;
        transform.position = posPlayer;
        StartCoroutine(spawnObjects());
    }

    IEnumerator spawnObjects()
    {
        foreach (amountItemsPerObjectSpawn item in itemsGameObjects)
        {
            for (int i = 0; i < item.amount; i++)
            {
                StartCoroutine(GetRandomPosition());
                Instantiate(item.item, randomPosItem, Quaternion.identity);
            }
        }
        yield return new WaitForSeconds(60);
        StartCoroutine(spawnObjects());
    }

    IEnumerator GetRandomPosition()
    {
        randomPosItem = posPlayer + new Vector3(UnityEngine.Random.Range(-radius.x / 2, radius.x / 2), 0, UnityEngine.Random.Range(-radius.z / 2, radius.z / 2));
        AlreadyGotThePositionGetTheY();
        yield return new WaitForSeconds(0);
    }

    private void AlreadyGotThePositionGetTheY()
    {
        if (Physics.Raycast(randomPosItem, dir * dist, 300))
        {
            randomPosItem.y = Terrain.activeTerrain.SampleHeight(randomPosItem);
        }
    }

    private void Update()
    {
        posPlayer = player.transform.position;
        transform.position = posPlayer;
    }
}
