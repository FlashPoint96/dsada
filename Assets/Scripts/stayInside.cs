﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stayInside : MonoBehaviour
{

    public Transform MinimapCam;
    public float MinimapSize;
    Vector3 TempV3;
    Vector3 centerPosition;
    private void Start()
    {
        TempV3 = this.transform.position;
        TempV3.y = transform.position.y;
        transform.position = TempV3;
        centerPosition = MinimapCam.transform.position;
    }


    void LateUpdate()
    {
        centerPosition.y = 50;
                
        float Distance = Vector3.Distance(transform.position, centerPosition);

        if (Distance > MinimapSize)
        {
            Vector3 fromOriginToObject = transform.position - centerPosition;

            fromOriginToObject *= MinimapSize / Distance;

            transform.position = centerPosition + fromOriginToObject;
        }
    }
}
