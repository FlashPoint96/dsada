﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAAnimations : MonoBehaviour
{
    private Animator anim;
    private string CurrentAnim;
    private StateMachine stateMachine;

    public string ENEMY_WALK     = "Walk";
    public string ENEMY_SHOUT    = "Shout";
    public string ENEMY_RUN      = "Run";
    public string ENEMY_ATTACK   = "Attack";
    public string ENEMY_GET_HIT  = "Get_Hit";
    public string ENEMY_DEAD     = "Dead";
    public string ENEMY_IDLE     = "Idle";

    private bool playthisOver = false;


    // Start is called before the first frame update
    void Start()
    {
        stateMachine = GetComponent<StateMachine>();
        anim = GetComponent<Animator>();
        CurrentAnim = ENEMY_IDLE;
    }

    public void ChangeAnim(string newState)
    {
        print("Soy el " + this.transform.gameObject + " y he intentado poner la animacion " + newState);
        if (playthisOver) return;
        if (anim == null) anim = GetComponent<Animator>();
        if (CurrentAnim == newState) return;

        anim.CrossFade(newState, 0.5f);
        CurrentAnim = newState;
    }

    public IEnumerator ChangeAnimOverOthers(string newState)
    {
        playthisOver = true;
        anim.Play(newState);
        yield return new WaitForSeconds(this.getAnimDuration());
        playthisOver = false;
    }


    public float getAnimDuration()
    {
        return anim.GetCurrentAnimatorStateInfo(0).length;
    }

    public bool getPlayingAnim(string animName)
    {
        print("SE ESTA REPRODUCIENDO " + animName + " SE ESTA REPRODUCIENDO ?" + anim.GetCurrentAnimatorStateInfo(0).IsName(animName));
        return anim.GetCurrentAnimatorStateInfo(0).IsName(animName);
    }

}
