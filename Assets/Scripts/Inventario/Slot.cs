﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Slot
{
    
    public ItemObject item;
    public int quantity;


    public Slot(ItemObject m_item,int m_quantity)
    {
        
        item = m_item;
        quantity = m_quantity;
    }

    public void AddQuantity(int val)
    {
        this.quantity += val;
    }




}
