﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonOnClickInv : MonoBehaviour
{
    [SerializeField]
    private Slot slot;
    private DisplayInventory display;

    private void Awake()
    {
        display = GameObject.Find("Slots").GetComponent<DisplayInventory>();
    }

    public void setSlot(Slot i) 
    {
        slot = i;
    }

    public void onClickDoThis()
    {
        display.itemFunction(slot);
    }

    public Slot getSlot()
    {

        return slot;
    }
}
