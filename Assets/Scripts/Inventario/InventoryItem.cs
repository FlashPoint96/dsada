﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName ="New Inventory", menuName ="Inventory/Inventory")]
public class InventoryItem : ScriptableObject
{
    public List<Slot> Slot = new List<Slot>();

    public void AddItem(ItemObject m_item,int m_quantity)
    {

        Debug.Log("elo estoy aqui " + m_item);
        for (int i = 0; i < Slot.Count; i++)
        {
            
            if (Slot[i].item.id == m_item.id && Slot[i].item.stackeable)
            {
                Slot[i].AddQuantity(m_quantity);
                return;
            }
        }
        Debug.Log("CREO UN SLOT");
        Slot.Add(new Slot(m_item, m_quantity));
           
    }

    public ItemObject getItemObject(Item item)
    {
        foreach (Slot s in Slot)
        {
            if(s.item._item.id == item.id)
            {
                return s.item;
            }
        }
        return new ItemObject(item);
    }

}
