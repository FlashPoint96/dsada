﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ToolTip : MonoBehaviour
{   

    public Text toolTipTxt;
    private RectTransform backgroundRectTransform;

    private void Awake()
    {
        backgroundRectTransform = transform.Find("background").GetComponent<RectTransform>();
    }

    public void ShowToolTip(string msg)
    {
        this.gameObject.SetActive(true);

        toolTipTxt.text = msg;


    }
    public void HideToolTip()
    {

        this.gameObject.SetActive(false);
    }
}
