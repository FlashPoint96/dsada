﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class ItemButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

     public ToolTip tooltipPopup;
     public InventoryItem inventory;
    public buttonOnClickInv onPointerSlot;

    private void Start()
    {
        
    }
    public void OnPointerEnter(PointerEventData eventData)
    {

        //enseño tooltip
        transform.SetAsLastSibling();
        tooltipPopup.ShowToolTip(GetItemMSG());

    }
    public void OnPointerExit(PointerEventData eventData)
    {
        
        tooltipPopup.HideToolTip();
    }

    public string GetItemMSG()
    {
        string atrMsg = " ";
        for (int i = 0; i < inventory.Slot.Count; i++)
        {

            if(onPointerSlot.getSlot().item  == inventory.Slot[i].item)
            {


                if (inventory.Slot[i].item._item.itemType == ItemType.Material)
                {
                    
                    atrMsg = "<color=yellow> " + inventory.Slot[i].item.Name.ToString() + "</color>"+"\n Quantity: " +
                    inventory.Slot[i].quantity.ToString() + "\n";
                    
                    return atrMsg;
                }else if (inventory.Slot[i].item._item.itemType == ItemType.Consumible)
                {
                    
                    atrMsg = "<color=yellow> " + inventory.Slot[i].item.Name.ToString() + "</color>" + "\n Quantity: " +
                    inventory.Slot[i].quantity.ToString() + "\n BonusHP: "+
                    inventory.Slot[i].item._item.hpPlus;
                    
                    return atrMsg;
                }
                else if (inventory.Slot[i].item._item.itemType == ItemType.Food)
                {

                    atrMsg = "<color=yellow> " + inventory.Slot[i].item.Name.ToString() + "</color>" + "\n Quantity: " +
                     inventory.Slot[i].quantity.ToString() + "\n BonusHP: " +
                     inventory.Slot[i].item._item.hpPlus;

                    return atrMsg;
                }
                else if (inventory.Slot[i].item._item.itemType == ItemType.Weapon)
                {
                    atrMsg = "<color=yellow> " + inventory.Slot[i].item._item.name + "</color>" + "\n Damage: " +
                  inventory.Slot[i].item._item.wso.damage+"\n Speed: "+
                  inventory.Slot[i].item._item.wso.timeBetweenShots;

                    return atrMsg;
                }



            }

        }

        return atrMsg;
    }
   

}
