﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayInventory : MonoBehaviour
{

    public InventoryItem inventory;
    public GameObject inventoryPrefab;
    public int X_START;
    public int Y_START;
    public int X_SPACE_BEETWEN_ITEM;
    public int NUMBER_OF_COLUMN;
    public int Y_SPACE_BEETWEN_ITEM;
    Dictionary<Slot, GameObject> items = new Dictionary<Slot, GameObject>();
    private GameObject fps;

    // Start is called before the first frame update
    void Start()
    {
        fps = GameObject.Find("FPSController");
        InitDisplay();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateDisplay();
    }
    public void UpdateDisplay()
    {

        for (int i = 0; i < inventory.Slot.Count; i++)
        {
            if (items.ContainsKey(inventory.Slot[i]))
            {
                if (inventory.Slot[i].quantity == 0)
                {
                    print("TIENE 0 OBJETOS");
                }
                items[inventory.Slot[i]].GetComponentInChildren<Text>().text = inventory.Slot[i].quantity.ToString("n0");
                items[inventory.Slot[i]].transform.GetChild(0).GetComponentInChildren<Image>().sprite = inventory.Slot[i].item.icon;
                items[inventory.Slot[i]].GetComponent<RectTransform>().localPosition = GetPosition(i);
                items[inventory.Slot[i]].transform.GetChild(2).transform.GetComponent<buttonOnClickInv>().setSlot(inventory.Slot[i]);

            }
            //Entra aqui cuando se crea un slot
            else 
            {               
                var obj = Instantiate(inventoryPrefab, Vector3.zero, Quaternion.identity, transform);
                obj.transform.GetChild(0).GetComponentInChildren<Image>().sprite = inventory.Slot[i].item.icon;
                obj.GetComponent<RectTransform>().localPosition = GetPosition(i);
                obj.GetComponentInChildren<Text>().text = inventory.Slot[i].quantity.ToString("n0");
                obj.transform.GetChild(2).transform.GetComponent<buttonOnClickInv>().setSlot(inventory.Slot[i]);
                items.Add(inventory.Slot[i], obj);
            }

        }
    }
    public void InitDisplay()
    {
        for (int i = 0; i < inventory.Slot.Count; i++)
        {
            
            var obj = Instantiate(inventoryPrefab, Vector3.zero, Quaternion.identity, transform);
            obj.transform.GetChild(0).GetComponentInChildren<Image>().sprite = inventory.Slot[i].item.icon;
            obj.GetComponent<RectTransform>().localPosition = GetPosition(i);
            obj.GetComponentInChildren<Text>().text = inventory.Slot[i].quantity.ToString("n0");
            obj.transform.GetChild(2).transform.GetComponent<buttonOnClickInv>().setSlot(inventory.Slot[i]);

            items.Add(inventory.Slot[i], obj);
        }

    }
    public Vector3 GetPosition(int i)
    {
        return new Vector3(X_START + (X_SPACE_BEETWEN_ITEM * (i % NUMBER_OF_COLUMN)), Y_START + (-Y_SPACE_BEETWEN_ITEM * (i / NUMBER_OF_COLUMN)), 0f);
    }

    public void itemFunction(Slot it)
    {
        if(it.item._item.itemType == ItemType.Food)
        {
            if(it.item._item.name.Equals("Agua")|| it.item._item.name.Equals("Cocacola"))
            {
                fps.GetComponent<FPSPlayerStatsAsigner>().WaterMe(it.item._item.hpPlus);
            }
            else
            {
                fps.GetComponent<FPSPlayerStatsAsigner>().FoodMe(it.item._item.hpPlus);

            }
            it.quantity--;
        }
        if (it.item._item.itemType == ItemType.Weapon)
        {
            bool aux = false;
            GameObject brazos = fps.gameObject.transform.GetChild(0).gameObject;
            for (int i = 0; i < brazos.transform.childCount; i++)
            {
                if(it.item._item.model.GetComponent<getWeaponSCO>().getWeapon().weaponID == brazos.transform.GetChild(i).GetComponent<getWeaponSCO>().getWeapon().weaponID)
                {
                    print("Ya tiene la mierda arma");
                    print(it.item._item.model.GetComponent<getWeaponSCO>().getWeapon().weaponID);
                    print(brazos.transform.GetChild(i).GetComponent<getWeaponSCO>().getWeapon().weaponID);
                    aux = true;
                }
            }
            if (!aux)
            {
                // LO COMENTO PA PROBAR UAN COSA
                if (brazos.transform.childCount <= 4)
                {
                    print(it.item._item.model);
                    GameObject newWeapon = Instantiate(it.item._item.model, new Vector3(0, 0, 0), Quaternion.identity);
                    newWeapon.transform.SetParent(brazos.transform, false);
                    newWeapon.transform.localPosition = new Vector3(0, -1.7f, 0);
                    newWeapon.transform.localScale = Vector3.one;
                    newWeapon.SetActive(false);
                    fps.GetComponent<FPSPlayerStatsAsigner>().fps.GameObjectsweapons.Add(it.item._item.model);
                    it.quantity--;
                }
                else
                {
                    fps.GetComponentInChildren<InventoryItem>().AddItem(new ItemObject(brazos.transform.GetChild(3).GetComponent<itemComponent>().getThisItem()), 1);
                    //fps.GetComponent<FPSPlayerStatsAsigner>().RemoveWeapon(brazos.transform.GetChild(3).gameObject);
                    print(it.item._item.model);
                    GameObject newWeapon = Instantiate(it.item._item.model, new Vector3(0, 0, 0), Quaternion.identity);
                    newWeapon.transform.SetParent(brazos.transform, false);
                    newWeapon.transform.localPosition = new Vector3(0, -1.7f, 0);
                    newWeapon.transform.localScale = Vector3.one;
                    newWeapon.SetActive(false);
                    fps.GetComponent<FPSPlayerStatsAsigner>().fps.GameObjectsweapons.Add(it.item._item.model);
                    it.quantity--;
                    //brazos.transform.GetChild(3).gameObject;
                }
            }
            else
            {
                print("Ya tieen esta arma");
            }
        }
        if(it.item._item.itemType == ItemType.Consumible)
        {
            if(it.item._item.wso != null)
            {
                if(it.item._item.name.Equals("Gasolina"))
                {
                    it.item._item.wso.gas += it.item._item.hpPlus;
                    it.quantity--;
                }
                else
                {
                    it.item._item.wso.maxShotsTotal += 5;
                    it.quantity--;
                }
            }
            if(it.item._item.name.Equals("venda"))
            {
                print("CURANDOME");
                fps.GetComponent<FPSPlayerStatsAsigner>().healMe(it.item._item.hpPlus);
                it.quantity--;
            }
            print(it.item._item.name+"NOMBRE");
        }
    }

    public void removeObject(Slot j)
    {
        if (items.ContainsKey(j))
        {
            GameObject.Destroy(items[j].transform.gameObject);
        }

    }
}
