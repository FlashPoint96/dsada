﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryController : MonoBehaviour
{
    //HABLAR SHAHRAZ SOBRE ESTOS IFS Y COSAS PA QUITAR EL GAMEOBJECT INVENTORY
    //HABLAR SHAHRAZ SOBRE ESTOS IFS Y COSAS PA QUITAR EL GAMEOBJECT INVENTORY
    //private GameObject inventory;
    public bool inventoryActive = false;
    private PauseController pause;
    private Camera camara;
    public GameObject di;
    public InventoryItem inventario;
    private GameObject slotsCraft;
    // Start is called before the first frame update
    void Start()
    {
        camara = this.transform.parent.root.transform.GetChild(0).gameObject.GetComponent<Camera>();
        pause = GameObject.FindGameObjectWithTag("pause").GetComponent<PauseController>();
        slotsCraft = GameObject.Find("Slots");
    }
    void Update()
    {
        cleanInventory();
        if (Input.GetKeyDown(KeyCode.E))
        {
            TryToPickUpItem();
        }
        if (Input.GetKeyDown(KeyCode.I) && inventoryActive == false && pause.pauseActive == false)
        {
            inventoryActive = true;
            /*for (int i = 0; i < slotsCraft.transform.childCount; i++)
            {
                print("UNO " + i);
                var colors = slotsCraft.transform.GetChild(i).GetChild(0).GetComponent<Button>().colors;
                colors.normalColor = slotsCraft.transform.GetChild(i).GetChild(0).GetComponentInChildren<CraftingButtonColors>().getColor();
                print("colors "+slotsCraft.transform.GetChild(i).GetChild(0).GetComponentInChildren<CraftingButtonColors>().getColor());
                slotsCraft.transform.GetChild(i).GetChild(0).GetComponent<Button>().colors = colors;
            }*/

        }
        else if (Input.GetKeyDown(KeyCode.I) && inventoryActive)
        {

            inventoryActive = false;

        }
        if (inventoryActive || pause.pauseActive)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

        }
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

    }

    private void TryToPickUpItem()
    {
        camara = this.transform.parent.root.transform.GetChild(0).gameObject.GetComponent<Camera>();
        //inventory = this.transform.parent.root.transform.GetChild(0).gameObject;

        RaycastHit hit;

        if (Physics.Raycast(camara.transform.position, camara.transform.forward, out hit, 20))
        {
            Debug.DrawLine(camara.transform.position, hit.point, Color.red, 5f);
            if (hit.transform.gameObject.tag == "item")
            {
                
                    var item = hit.transform.gameObject.GetComponent<itemComponent>();
                    if (item)
                    {
                        //print(item.getThisItem());
                        if (inventario.Slot.Count != 24)
                        {

                            inventario.AddItem(new ItemObject (item.getThisItem()), 1);
                            GameObject.Destroy(hit.transform.gameObject);
                        }
                    }

               }

            StartCoroutine(TryWithCollider());
            
        }

    }

    IEnumerator TryWithCollider()
    {
        this.transform.parent.root.GetChild(6).GetComponent<CapsuleCollider>().enabled = true;
        yield return new WaitForSeconds(2f);
        this.transform.parent.root.GetChild(6).GetComponent<CapsuleCollider>().enabled = false;
    }
    public InventoryItem getInventoryItem()
    {
        return inventario;
    }
    public void cleanInventory()
    {
        for (int i = 0; i < inventario.Slot.Count; i++)
        {
            if (inventario.Slot[i].quantity <= 0)
            {
                di.GetComponent<DisplayInventory>().removeObject(inventario.Slot[i]);
                inventario.Slot.Remove(inventario.Slot[i]);
            }
        }

    }

    //Limpiar el inventario cada vez que se cierra la aplicacion
    /*private void OnApplicationQuit()
    {
        inventario.Slot.Clear();
    }*/
}
