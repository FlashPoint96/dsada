﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageEnemy : MonoBehaviour
{

    private bool isComboAttack = false;
    
    private void OnTriggerEnter(Collider other)
    {
        float damage = GetComponentInParent<FPSPlayerWeaponController>().GetFirstActiveChild().GetComponent<getWeaponSCO>().getWeapon().damage+GetComponentInParent<FPSPlayerStatsAsigner>().getPlayerStats().Damage;
        if (isComboAttack)
        {
            damage *= 2;
        }
        if(GetComponentInParent<FPSPlayerStatsAsigner>().getPlayerStats().CDPorce> Random.Range(0, 100))
        {
            print("Le ha tocado critico");
            damage *= 2;
        }
        print(other.tag);
        if (other.gameObject.tag == "Enemy")
        {
        
            GetComponentInParent<InteractionController>().setEnemyTarget(other.gameObject.GetComponent<IAStats>());
            print("le pegaria un ostia de " + damage + "sIN QUITAR ESCUDO y el ataque era con un combo?"+isComboAttack);
            other.gameObject.GetComponent<IAStats>().damageThis(damage, this.gameObject);
            gameObject.GetComponent<SphereCollider>().enabled = false;
        }
    }

    public void setAsComboAttack()
    {
        isComboAttack = true;
    }

    public void setAsNormalAttack()
    {
        isComboAttack = false;
    }
}
