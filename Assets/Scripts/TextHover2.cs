﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TextHover2 : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    
    public Text text;

    public void OnPointerEnter(PointerEventData eventData)
    {
       //Or however you do your color
        text.color = Color.red;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Or however you do your color
        text.color = Color.white;
    }



}

