﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DisplayCrafting : MonoBehaviour
{
    //public CraftingRecipe recipes;
    public GameObject SlotCraftingPrefab;
    public int X_START;
    public int Y_START;
    public int X_SPACE_BEETWEN_ITEM;
    public int NUMBER_OF_COLUMN;
    public int Y_SPACE_BEETWEN_ITEM;
    Dictionary<CraftingRecipe, GameObject> items = new Dictionary<CraftingRecipe, GameObject>();
    private GameObject fps;
    public CraftingSystem crafting;


    // Start is called before the first frame update
    void Start()
    {
        fps = GameObject.Find("FPSController");

        InitDisplay();

    }

    // Update is called once per frame
    void Update()
    {
        

    }

    private void UpdateDisplay()
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            var colors = this.transform.GetChild(i).GetChild(0).GetComponent<Button>().colors;
            colors.normalColor = this.transform.GetChild(i).GetChild(0).GetComponentInChildren<CraftingButtonColors>().getColor();
            this.transform.GetChild(i).GetChild(0).GetComponent<Button>().colors = colors;
        }

    }

    public void InitDisplay()
    {
        for (int i = 0; i < crafting.justRecipes.Count; i++)
        {
            var obj = Instantiate(SlotCraftingPrefab, Vector3.zero, Quaternion.identity, transform);
            obj.transform.GetChild(0).GetChild(0).GetComponentInChildren<Image>().sprite = crafting.justRecipes[i].getItemCraft().icon;
            obj.transform.GetChild(0).GetChild(1).GetComponentInChildren<Text>().text = crafting.justRecipes[i].getItemCraft().Name;
            obj.GetComponent<RectTransform>().localPosition = GetPosition(i);
            obj.transform.GetChild(0).GetChild(2).GetComponentInChildren<Text>().text = "";
            for (int j = 0; j < crafting.justRecipes[i].itemsNeedForCrafting.Count; j++)
            {
                obj.transform.GetChild(0).GetChild(2).GetComponentInChildren<Text>().text += " "+crafting.justRecipes[i].itemsNeedForCrafting[j].Item.Name + " " + crafting.justRecipes[i].itemsNeedForCrafting[j].Amount.ToString("n0");
//                obj.transform.GetChild(0).GetChild(2).GetComponentInChildren<Text>().text += crafting.justRecipes[i].itemsNeedForCrafting[j].Item.Name +" aa" +crafting.justRecipes[i].itemsNeedForCrafting[i].Amount.ToString("n0");
            }
            obj.GetComponentInChildren<CraftThisItem>().setRC(crafting.justRecipes[i]);
            //obj.transform.GetChild().transform.GetComponent<buttonOnClickInv>().setSlot(inventory.Slot[i]);
            items.Add(crafting.justRecipes[i], obj);
        }

    }

    public Vector3 GetPosition(int i)
    {
        return new Vector3(X_START + (X_SPACE_BEETWEN_ITEM * (i % NUMBER_OF_COLUMN)), Y_START + (-Y_SPACE_BEETWEN_ITEM * (i / NUMBER_OF_COLUMN)), 0f);
    }
}
