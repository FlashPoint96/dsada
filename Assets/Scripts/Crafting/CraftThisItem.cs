﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftThisItem : MonoBehaviour
{
    [SerializeField]
    private CraftingRecipe rc;

    [SerializeField]
    private Item item;

    private void Awake()
    {
        //rc.SetItemCraft(new ItemObject(item));
    }
    public CraftingRecipe getThisRC()
    {
        return rc;
    }

    public void setRC(CraftingRecipe cr)
    {
        this.rc = cr;
    }

    void OnMouseOver()
    {
        //If your mouse hovers over the GameObject with the script attached, output this message
        Debug.Log("Mouse is over GameObject.");
    }

    public void onClickDoThis()
    {
        this.GetComponent<CraftingButtonColors>().getthisRC().craftThisItem(rc);
    }
}
