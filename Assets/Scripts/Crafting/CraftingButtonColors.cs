﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CraftingButtonColors : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    private InventoryController iv;
    [SerializeField] private CraftingSystem crafteo;
    private Color defaultColor;
    private GameObject playerthings;
    // Start is called before the first frame update
    void Start()
    {
        playerthings = GameObject.Find("PlayerThings");

        var colors = this.GetComponent<Button>().colors;
        colors = this.GetComponent<Button>().colors;
        defaultColor = colors.normalColor;


        iv = GameObject.Find("InventoryManager").GetComponent<InventoryController>();
        crafteo = GameObject.Find("FPSController").GetComponent<CraftingSystem>();
        //this.GetComponent<Button>().onClick.AddListener(crafteo.craftThisItem(this.GetComponent<CraftThisItem>().getThisRC()));
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Button>().enabled == false)
        {
            this.GetComponent<Image>().color = defaultColor;
            /*var colors = this.GetComponent<Button>().colors;
            colors.normalColor = defaultColor;
            colors.highlightedColor = defaultColor;
            GetComponent<Button>().colors = colors;
        */
        }
        else
        {
            this.GetComponent<Image>().color = Color.white;
        }


    }

    public CraftingSystem getthisRC()
    {
        return crafteo;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //this.GetComponent<Image>().color = Color.white;
        var colors = this.GetComponent<Button>().colors;

        if (this.GetComponent<Button>().enabled == false)
        {
            
            colors.normalColor = Color.white;

            GetComponent<Button>().colors = colors;
        }
        else
        {

            colors.normalColor = defaultColor;
            this.GetComponent<Image>().color = Color.white;
            GetComponent<Button>().colors = colors;

        }

        }

    public void OnPointerEnter(PointerEventData eventData)
    {
        var colors = this.GetComponent<Button>().colors;
        if (this.GetComponent<CraftThisItem>().getThisRC().isCrafteable)
        {
            if (this.GetComponent<CraftThisItem>().getThisRC().CanCraft(iv.inventario))
            {
                this.GetComponent<Button>().enabled = true;
                colors = this.GetComponent<Button>().colors;
                colors.normalColor = Color.green;
                colors.highlightedColor = Color.green;
                colors.pressedColor = defaultColor;
                colors.selectedColor = defaultColor;
                GetComponent<Button>().colors = colors;

            }
            else
            {


                this.GetComponent<Button>().enabled = false;
                this.GetComponent<Image>().color = defaultColor;
                /*colors = this.GetComponent<Button>().colors;
                colors.normalColor = defaultColor;
                colors.highlightedColor = defaultColor;
                GetComponent<Button>().colors = colors;
               */
            }


        }
        else
        {
            
            this.GetComponent<Button>().enabled = true;
            colors = this.GetComponent<Button>().colors;
            colors.normalColor = Color.blue;
            colors.highlightedColor = Color.blue;
            GetComponent<Button>().colors = colors;

        }
    }
    public Color getColor()
    {
        return defaultColor;
    }
}
