﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]
public class SetItemsCraft
{
    public Item item;
    public int Amount;
}

[System.Serializable]
public class SetCraftingItems
{
    public CraftingRecipe cr;
    public Item craftingItem;
    public int amount;
    public List<SetItemsCraft> items;
}


public class CraftingSystem : MonoBehaviour
{
    [SerializeField]
    //private GameObject[] craftingButtons;
    private InventoryController iv;
    [SerializeField] private List<SetCraftingItems> setRecipes = new List<SetCraftingItems>();
    [SerializeField]public List<CraftingRecipe> justRecipes = new List<CraftingRecipe>();
    // Start is called before the first frame update
    void Start()
    {
        iv = this.GetComponentInChildren<InventoryController>();
        InitCraftingRecipes();
    }

    private void InitCraftingRecipes()
    {
        foreach (SetCraftingItems s in setRecipes)
        {
            s.cr.SetItemCraft(iv.inventario.getItemObject(s.craftingItem));
            s.cr.itemsNeedForCrafting.Clear();
            foreach (SetItemsCraft item in s.items)
            {
                s.cr.itemsNeedForCrafting.Add(new itemsForCrafit(iv.inventario.getItemObject(item.item), item.Amount));
            }
            s.cr.amount = s.amount;
        }
    }

    public void Update()
    {
        //eS CON UN EVENTO AL ABRIR EL INVENTARIO
        updateCraftButtons();
    }
    public void craftThisItem
        (CraftingRecipe cr)
    {
        if (cr.isCrafteable)
        {
            cr.Craft(iv.inventario);
        }
    }

    public void updateCraftButtons()
    {
        /*foreach(GameObject g in craftingButtons)
        {
            if (g.GetComponent<CraftThisItem>().getThisRC().isCrafteable)
            {
                if (g.GetComponent<CraftThisItem>().getThisRC().CanCraft(iv.inventario))
                {
                    g.GetComponent<Button>().enabled = true;
                    g.GetComponent<Image>().color = Color.green;
                }
                else
                {
                    g.GetComponent<Button>().enabled = false;
                    g.GetComponent<Image>().color = Color.grey;
                }
            }
            else
            {
                g.GetComponent<Button>().enabled = true;
                g.GetComponent<Image>().color = Color.blue;
            }
        }*/
    }

    public List<CraftingRecipe> getRecipes()
    {
        return justRecipes;
    }

}
