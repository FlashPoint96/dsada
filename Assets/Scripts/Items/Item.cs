﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    Consumible,
    Weapon,
    Material,
    Food,
    Default


}
public  class Item : ScriptableObject
{
    public int id;
    public string name;
    public ItemType itemType;
    public Sprite icon;
    public bool stackeable;
    [TextArea(15, 20)]
    public string description;
    public GameObject model;
    public int hpPlus;
    public WeaponScriptableObject wso;


}

