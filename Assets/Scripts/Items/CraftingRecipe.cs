﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class itemsForCrafit
{
    public ItemObject Item;
    public int Amount;

    public itemsForCrafit(ItemObject m_item, int m_quantity)
    {

        Item = m_item;
        Amount = m_quantity;
    }
}
[CreateAssetMenu(fileName = "CraftingRecipe", menuName = "Items/CraftingRecipe", order = 1)]
public class CraftingRecipe : ScriptableObject
{
    [SerializeField]private ItemObject itemCraft;
    public int amount;
    public List<itemsForCrafit> itemsNeedForCrafting;
    public bool isCrafteable;

    public bool CanCraft(InventoryItem iv)
    {
        int mats = 0;
        foreach(itemsForCrafit it in itemsNeedForCrafting)
        {
            foreach (Slot s in iv.Slot)
            {
                if(it.Item.id == s.item.id)
                {
                    if (s.quantity >= it.Amount)
                    {
                        mats++;
                    }       
                }
            }
        }
        if (mats == itemsNeedForCrafting.Count)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void Craft(InventoryItem iv)
    {
        if (CanCraft(iv))
        {
            foreach (itemsForCrafit it in itemsNeedForCrafting)
            {
                foreach (Slot s in iv.Slot)
                {
                    if (it.Item.id == s.item.id)
                    {
                        if (s.quantity >= it.Amount)
                        {
                            s.quantity -= it.Amount;
                        }
                    }
                }
            }
            iv.AddItem(itemCraft, amount);
        }
    }

    public void SetItemCraft(ItemObject it)
    {
        this.itemCraft = it;
    }

    public ItemObject getItemCraft()
    {
        return itemCraft;
    }

}
