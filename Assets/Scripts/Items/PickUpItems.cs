﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItems : MonoBehaviour
{
    private GameObject inventory;
    private GameObject backpack;
    private Camera camara;
    [SerializeField]
    private List<Item> itemsOnArray;

    public InventoryItem inventario;

    // Start is called before the first frame update
    void Start()
    {
        camara = this.transform.GetChild(0).gameObject.GetComponent<Camera>();
        inventory = this.transform.GetChild(0).gameObject;
        backpack = this.transform.GetChild(1).gameObject;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            TryToPickUpItem();
        }
    }

    private void TryToPickUpItem()
    {
        camara = this.transform.GetChild(0).gameObject.GetComponent<Camera>();
        inventory = this.transform.GetChild(0).gameObject;

        RaycastHit hit;

        if (Physics.Raycast(camara.transform.position, camara.transform.forward, out hit, 500))
        {
            Debug.DrawLine(camara.transform.position, hit.point, Color.red, 5f);
            if (hit.transform.gameObject.tag == "item")
            {
                if (inventory.gameObject.transform.childCount <= 3)
                {
                    //Item objeto = hit.transform.gameObject.GetComponent<itemComponent>().getThisItem();
                    //GameObject.Destroy(hit.transform.gameObject);
                    //itemsOnArray.Add(item);
                    var item = hit.transform.gameObject.GetComponent<itemComponent>();
                    if (item)
                    {
                        inventario.AddItem(new ItemObject(item.getThisItem()), 1);
                        GameObject.Destroy(hit.transform.gameObject);
                    }
                   
                }
                else
                {
                    hit.transform.gameObject.SetActive(false);
                    hit.transform.SetParent(inventory.transform, false);
                    hit.transform.localPosition = inventory.transform.localPosition;
                    hit.transform.localEulerAngles = inventory.transform.localEulerAngles;
                    hit.transform.localScale = Vector3.one;
                    Destroy(hit.transform.GetComponent<CapsuleCollider>());
                }
            }
        }

    }
}
