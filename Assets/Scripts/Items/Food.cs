﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "New Food", menuName = "Items/Create Food", order = 3)]
public class Food : Item
{
    public void Awake()
    {
        itemType = ItemType.Food;
    }

}
