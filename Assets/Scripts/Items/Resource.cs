﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "New Material", menuName = "Items/Create Material", order = 1)]
public class Resource : Item
{
    public void Awake()
    {
        itemType = ItemType.Material;
    }

}
