﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "New Consumible", menuName = "Items/Create Consumible", order =2)]
public class Consumible : Item
{
    public int HP;

    public void Awake()
    {
        itemType = ItemType.Consumible;
    }

}

