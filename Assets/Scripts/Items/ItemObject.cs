﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemObject
{
    public string Name;
    public int id;
    public Sprite icon;
    public bool stackeable;
    public Item _item;
    public GameObject model;

    public ItemObject(Item m_item)
    {
        Name = m_item.name;
        id = m_item.id;
        icon = m_item.icon;
        stackeable = m_item.stackeable;
        _item = m_item;
        model = m_item.model;
    }


}
