﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Items/Create Weapon", order = 4)]
public class WeaponScriptableObject : Item
{
    public int weaponID;
    public bool isRanged;
    public float cdBetweenActions;
    public float damage;
    public int maxShotsTotal;
    public int maxShotsCharger;
    public int actualShotsInCharger;
    public float timeBetweenShots;
    public float CDcomboAttack;
    public float dispersion;
    public bool isSemiAuto;
    public bool haveCombo;
    public int weaponRange;
    public float momentos;
    public float weaponZoom;
    public int numberOfShosOnSemi;
    public bool createsLight;
    public float gas;
    public AudioClip attackAudioClip;
    public AudioClip reloadClip;
    public Sprite bulletIcon;
    public Sprite gunIcon;
    public GameObject VFX;

}
