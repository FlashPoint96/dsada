﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
[System.Serializable]
public class SecureZone
{
    public String zoneName;
    public Vector3 zoneCenter;
    public Vector3 zoneRadius;
    public bool isActive;
}
[System.Serializable]
public class EnemyZones
{
    public String EnemyZoneName;
    public int EnemyStartNumber;
    public float EnemySpawnFreq;
    public BoxCollider EnemyBoxCollider;
    public List<ZombieType> EnemyZombieTypesOnThatZone;
}

[System.Serializable]
public class ZombieType
{
    public String ZombieName;
    public GameObject ZombieGameObject;
    [Range(0,100)]
    public float AppearPorcentatgeOnThatZone;
}

public class EnemySpawner : MonoBehaviour
{    
    private Vector3 posPlayer;
    private Vector3 radius;
    private Vector3 radius2;
    private BoxCollider bc;
    private BoxCollider spawnZone;
    private GameObject zoneLvlFather;

    [SerializeField]
    private List<EnemyZones> EnemyZoneLvls = new List<EnemyZones>();

    [SerializeField]
    private List<ZombieType> enemyGameObjectsAux = new List<ZombieType>();

    [SerializeField]
    private List<SecureZone> SecureZones = new List<SecureZone>();

    private GameObject player;

    private EnemyPool ep;

    [Header("INFO")]
    private string actualEnemyName;
    private Vector3 enemySpawnPosition;
    private float dist = 10000;
    private Vector3 dir = new Vector3(0, -1, 0);
    [SerializeField]
    private List<GameObject> enemiesSpawned = new List<GameObject>();
    private Vector3 enemyPosAux = new Vector3(100, 0, 100);
    private float EnemySpawnFreq;
    private int StarterZombies;
    [SerializeField]
    private List<ZombieType> activeZombieTypeZone;
    public static EnemySpawner instance;

    private bool zone1, zone2, zone3, zone4, zone5, zone2B, zone4B, zone5B;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        load();
        StartCoroutine(SpawningZombiesLoop());
    }

    private void load()
    {
        bc = this.GetComponent<BoxCollider>();
        spawnZone = this.GetComponentInChildren<BoxCollider>();
        ep = this.GetComponent<EnemyPool>();
        zoneLvlFather = GameObject.Find("LvLZones").gameObject;
        player = GameObject.Find("FPSController").gameObject;
        posPlayer = player.transform.localPosition;
        spawnZone.center = player.transform.position;
        radius = new Vector3(50, 100, 50);
        radius2 = new Vector3(500, 100, 500);
        bc.size = radius;
        bc.isTrigger = true;
        bc.center = new Vector3(0,0,0);
        posPlayer = player.transform.position;
        transform.position = posPlayer;
        /*SecureZone splayer = new SecureZone();
        splayer.zoneName = "PlayerZone";
        splayer.isActive = true;
        splayer.zoneCenter = posPlayer;
        splayer.zoneRadius = new Vector3(100, 100, 100);
        SecureZones.Add(splayer);*/
        for (int i = 0; i < zoneLvlFather.transform.childCount; i++)
        {
            EnemyZoneLvls[i].EnemyZoneName = zoneLvlFather.transform.GetChild(i).gameObject.name;
            EnemyZoneLvls[i].EnemyBoxCollider = zoneLvlFather.transform.GetChild(i).gameObject.GetComponent<BoxCollider>();
            //ZoneLvls.Add(zoneLvlFather.transform.GetChild(i).gameObject.GetComponent<BoxCollider>());
        }
        foreach (EnemyZones item in EnemyZoneLvls)
        {
            //item.EnemyStartNumber = 5;
            for (int i = 0; i < 4; i++)
            {
                if(item.EnemyZombieTypesOnThatZone == null)
                {
                    item.EnemyZombieTypesOnThatZone = new List<ZombieType>();
                }
                item.EnemyZombieTypesOnThatZone[i].ZombieName = enemyGameObjectsAux[i].ZombieName;
                item.EnemyZombieTypesOnThatZone[i].ZombieGameObject = enemyGameObjectsAux[i].ZombieGameObject;
            }
        }

        foreach (EnemyZones item in EnemyZoneLvls)
        {
            if(item.EnemyZoneName.Equals(CheckWhichZoneName(player.transform.position)))
            {
                EnemySpawnFreq = item.EnemySpawnFreq;
                StarterZombies = item.EnemyStartNumber;
                activeZombieTypeZone = item.EnemyZombieTypesOnThatZone;
                if (CheckWhichZoneName(player.transform.position).Equals("ZoneLvL1"))
                {
                    zone1 = true;
                }else if (CheckWhichZoneName(player.transform.position).Equals("ZoneLvL2")){
                    zone2 = true;
                }else if (CheckWhichZoneName(player.transform.position).Equals("ZoneLvL3"))
                {
                    zone3 = true;
                }
                else if (CheckWhichZoneName(player.transform.position).Equals("ZoneLvL4"))
                {
                    zone4 = true;
                }
                else if (CheckWhichZoneName(player.transform.position).Equals("ZoneLvL5"))
                {
                    zone5 = true;
                }
            }
        }

        for (int i = 0; i < StarterZombies; i++)
        {
            SpawningZombiesSolo("noZone");
        }
    }

    IEnumerator SpawningZombiesLoop()
    {
        getRandomPosition();
        GameObject auxEnemy = EnemyPool.instance.getEnemy(actualEnemyName);
        if (auxEnemy)
        {
            auxEnemy.transform.position = enemySpawnPosition;
            auxEnemy.transform.rotation = Quaternion.identity;
            auxEnemy.SetActive(true);
            enemiesSpawned.Add(auxEnemy);
        }
        yield return new WaitForSeconds(30);
        StartCoroutine(SpawningZombiesLoop());
    }
    
    public void SpawningZombiesSolo(string zoneName)
    {
        if (zoneName.Equals("noZone"))
        {
            getRandomPosition();
            GameObject auxEnemy = EnemyPool.instance.getEnemy(actualEnemyName);
            //print(auxEnemy);
            if (auxEnemy)
            {
                auxEnemy.transform.position = enemySpawnPosition;
                auxEnemy.transform.rotation = Quaternion.identity;
                auxEnemy.SetActive(true);
                enemiesSpawned.Add(auxEnemy);
            }
        }
        else
        {
            getRandomPositionOnZone(zoneName);
            GameObject auxEnemy = EnemyPool.instance.getEnemy(actualEnemyName);
            //print(auxEnemy);
            if (auxEnemy)
            {
                auxEnemy.transform.position = enemySpawnPosition;
                auxEnemy.transform.rotation = Quaternion.identity;
                auxEnemy.SetActive(true);
                enemiesSpawned.Add(auxEnemy);
            }
        }
    }

    private void CheckWhichZombie()
    {
        float aux = 0;
        switch (CheckWhichZoneName(enemySpawnPosition))
        {
            case "ZoneLvL1":
                GetZombieName(aux, 0);
                break;
            case "ZoneLvL2":
                GetZombieName(aux, 1);
                break;
            case "ZoneLvL3":
                GetZombieName(aux, 2);
                break;
            case "ZoneLvL4":
                GetZombieName(aux, 3);
                break;
            case "ZoneLvL5":
                GetZombieName(aux, 6);
                break;
            case "ZoneLvL2B":
                GetZombieName(aux, 4);
                break;
            case "ZoneLvL5B":
                GetZombieName(aux, 6);
                break;
            case "ZoneLvL4B":
                GetZombieName(aux, 5);
                break;
            default:
                GetZombieName(aux, 0);
                break;

        }
    }

    private void GetZombieName(float aux, int v)
    {
        aux = UnityEngine.Random.RandomRange(0, 100);
        if (EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[0].AppearPorcentatgeOnThatZone >= aux)
        {
            actualEnemyName = EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[0].ZombieName;
        }
        else if (aux > EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[0].AppearPorcentatgeOnThatZone && EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[0].AppearPorcentatgeOnThatZone + EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[1].AppearPorcentatgeOnThatZone >= aux)
        {
            actualEnemyName = EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[1].ZombieName;
        }
        else if (aux > EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[0].AppearPorcentatgeOnThatZone + EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[1].AppearPorcentatgeOnThatZone && EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[0].AppearPorcentatgeOnThatZone + EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[1].AppearPorcentatgeOnThatZone+EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[2].AppearPorcentatgeOnThatZone >= aux)
        {
            actualEnemyName = EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[2].ZombieName;
        }
        else if (aux > EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[0].AppearPorcentatgeOnThatZone + EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[1].AppearPorcentatgeOnThatZone + EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[2].AppearPorcentatgeOnThatZone && EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[0].AppearPorcentatgeOnThatZone + EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[1].AppearPorcentatgeOnThatZone + EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[2].AppearPorcentatgeOnThatZone+EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[3].AppearPorcentatgeOnThatZone >= aux)
        {
            actualEnemyName = EnemyZoneLvls[v].EnemyZombieTypesOnThatZone[3].ZombieName;
        }
        else
        {
            print("NO SE" + aux + " ZONA "  +v);
        }
    }

    private bool AlreadyGotThePositionGetTheY()
    {
        if (Physics.Raycast(enemySpawnPosition, dir * dist, 10000))
        {
            Debug.DrawRay(enemySpawnPosition, dir * dist, Color.yellow,120f);
            enemySpawnPosition.y = Terrain.activeTerrain.SampleHeight(enemySpawnPosition);
            return true;
        }
        else
        {
            return false;
        }
    }

    void getRandomPosition()
    {
        //enemySpawnPosition =posPlayer + UnityEngine.Random.onUnitSphere * UnityEngine.Random.Range(50, 200);

        enemySpawnPosition = posPlayer + new Vector3(UnityEngine.Random.Range(-radius2.x / 2, radius2.x / 2), 0, UnityEngine.Random.Range(-radius2.z / 2, radius2.z / 2));
        if (CheckIfInSameZone(enemySpawnPosition))
        {
            getRandomPosition();
        }
        else
        {
            if (CheckIfSecureZone(enemySpawnPosition) && CheckWhichZone(enemySpawnPosition))
            {

            if (!AlreadyGotThePositionGetTheY())
            {
                print("NO LE HA DADO A NADA EL RAYCAST");
            }

            CheckWhichZombie();
            }
            else
            {
                getRandomPosition();
            }
        }
    }

    void getRandomPositionOnZone(string zone)
    {
        //enemySpawnPosition =posPlayer + UnityEngine.Random.onUnitSphere * UnityEngine.Random.Range(50, 200);

        enemySpawnPosition = posPlayer + new Vector3(UnityEngine.Random.Range(-radius2.x / 2, radius2.x / 2), 0, UnityEngine.Random.Range(-radius2.z / 2, radius2.z / 2));
        if (CheckIfInSameZone(enemySpawnPosition))
        {
            getRandomPositionOnZone(zone);
        }
        else
        {
            if (CheckIfSecureZone(enemySpawnPosition) && CheckWhichZone(enemySpawnPosition))
            {
                if (CheckWhichZoneName(enemySpawnPosition).Equals(zone))
                {
                    if (!AlreadyGotThePositionGetTheY())
                    {
                        print("NO LE HA DADO A NADA EL RAYCAST");
                    }
                    CheckWhichZombie();
                }
                else
                {
                    getRandomPositionOnZone(zone);
                }
            }
            else
            {
                getRandomPositionOnZone(zone);
            }
        }
    }

    /* IEnumerator GetRandomPosition()
     {
         enemySpawnPosition = posPlayer + new Vector3(UnityEngine.Random.Range(-radius.x / 2, radius.x / 2), 0, UnityEngine.Random.Range(-radius.z / 2, radius.z / 2));
         print("GETTIN A POSITION");
         if (CheckIfSecureZone(enemySpawnPosition) && CheckWhichZone(enemySpawnPosition))
         {
             print("GOT A POSITION");
             AlreadyGotThePositionGetTheY();
             CheckWhichZombie();
             yield return new WaitForSeconds(1);
         }
         else
         {
             yield return new WaitForSeconds(0);
             StartCoroutine(GetRandomPosition());
         }
     }*/
    IEnumerator WaitXSeconds(float time)
    {
        yield return new WaitForSeconds(time);
    }

    private bool CheckIfSecureZone(Vector3 newEnemy)
    {
        int AUX = 0;
        foreach (SecureZone s in SecureZones)
        {
                //s.zoneRadius.x += s.zoneCenter.x;
                //s.zoneRadius.z += s.zoneCenter.z;
                //s.zoneRadius.y = 100;

                if (!isInTwoVectors(s.zoneCenter, s.zoneRadius, newEnemy))
                {
                    AUX++;
                }
        }
        if (AUX == SecureZones.Count)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool CheckWhichZone(Vector3 v3)
    {
        foreach (EnemyZones b in EnemyZoneLvls)
        {
            if (b.EnemyBoxCollider.bounds.Contains(v3))
            {
                return true;
            }
        }
        return false;
    }
    private bool notInZone(Vector3 v3,BoxCollider bx)
    {
        if (bx.bounds.Contains(v3))
        {
            return true;
        }
        return false;
    }
    private bool CheckIfInSameZone(Vector3 v3)
    {
        if (spawnZone.bounds.Contains(v3))
        {
            print("si LO CONTIENE");
            return true;
        }
        return false;
    }
    bool PointInOABB(Vector3 point, BoxCollider box)
    {
        point = box.transform.InverseTransformPoint(point) - box.center;

        float halfX = (box.size.x * 0.5f);
        float halfY = (box.size.y * 0.5f);
        float halfZ = (box.size.z * 0.5f);
        if (point.x < halfX && point.x > -halfX &&
           point.y < halfY && point.y > -halfY &&
           point.z < halfZ && point.z > -halfZ)
            return true;
        else
            return false;
    }

    public string CheckWhichZoneName(Vector3 v3)
    {
        foreach (EnemyZones b in EnemyZoneLvls)
        {
            if (b.EnemyBoxCollider.bounds.Contains(v3))
            {
                return b.EnemyZoneName;
            }
        }
        return "LA PUTA" + v3;
    }

    private void Update()
    {
        posPlayer = player.transform.position;
        transform.position = posPlayer;
        playerZoneTransform();
        if (enemiesSpawned.Count>= 1)
        {
            CheckIfEnemyOutOfZone();
        }
    }

    private void CheckIfEnemyOutOfZone()
    {
        List<GameObject> auxDelete = new List<GameObject>();

        foreach (GameObject enemy in enemiesSpawned)
        {
           if (!CheckWhichZone(enemy.transform.position))
            {
                //Devolverlo al pool
                auxDelete.Add(enemy);
                EnemyPool.instance.returnEnemy(enemy);
            }
            //Esto funciona pero los borra de una al salir de una zona y entrar a otra
            /*if (CheckWhichZoneName(player.transform.position) != enemy.ZoneName)
            {
                //enemiesSpawned.RemoveAt(n);
                auxDelete.Add(enemy);
                ep.returnEnemy(enemy.Zombie);
            }*/
        }
        foreach (GameObject enemyAux in auxDelete)
        {
            if (enemiesSpawned.Contains(enemyAux))
            {
                enemiesSpawned.Remove(enemyAux);
                SpawningZombiesSolo("noZone");
            }
        }
        auxDelete.Clear();

    }

    public void returnEnemyAndDelete(GameObject enemyAux,GameObject elQueLoMANDA)
    {
        print("ME HAN MANDADO BORRAR ESTE GAMEOBJECTE DE PARTE DE " + elQueLoMANDA.gameObject.name + " Y SOY EL ZOMBIE" +enemyAux + " Y ESTABA EN LA POSICION " +enemyAux.gameObject.transform.position);
        enemiesSpawned.Remove(enemyAux);
        EnemyPool.instance.returnEnemy(enemyAux);
        SpawningZombiesSolo("noZone");
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawCube(posPlayer, radius);
        foreach (SecureZone s in SecureZones)
        {
            Gizmos.DrawCube(s.zoneCenter, s.zoneRadius);
        }
    }

    bool IsCBetweenAB(Vector3 A, Vector3 B, Vector3 C)
    {
        return Vector3.Dot((B - A).normalized, (C - B).normalized) < 0f && Vector3.Dot((A - B).normalized, (C - A).normalized) < 0f;
    }
    public bool isInTwoVectors(Vector3 pos1, Vector3 pos2, Vector3 check)
    {
        float minX = Mathf.Min(pos1.x, pos2.x);
        float maxX = Mathf.Max(pos1.x, pos2.x);
        float minY = Mathf.Min(pos1.y, pos2.y);
        float maxY = Mathf.Max(pos1.y, pos2.y);
        float minZ = Mathf.Min(pos1.z, pos2.z);
        float maxZ = Mathf.Max(pos1.z, pos2.z);
        return check.x >= minX && check.x <= maxX && check.y >= minY && check.y <= maxY
            && check.z >= minZ && check.z <= maxZ;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "ZoneLvL2" && !zone2)
        {
            print("HE COLISIONADO CON Y SOY EL ENEMYASPAWNER" + other.gameObject.name);
            foreach (EnemyZones item in EnemyZoneLvls)
            {
                if (item.EnemyZoneName.Equals(other.gameObject.name))
                {
                    EnemySpawnFreq = item.EnemySpawnFreq;
                    StarterZombies = item.EnemyStartNumber;
                    activeZombieTypeZone = item.EnemyZombieTypesOnThatZone;
                }
            }

            for (int i = 0; i < StarterZombies; i++)
            {
                print(CheckWhichZoneName(player.transform.position));
                SpawningZombiesSolo(other.gameObject.name);
            }
            zone2 = true;
        }
        if (other.gameObject.name == "ZoneLvL3" && !zone3)
        {
            print("HE COLISIONADO CON Y SOY EL ENEMYASPAWNER" + other.gameObject.name);
            foreach (EnemyZones item in EnemyZoneLvls)
            {
                if (item.EnemyZoneName.Equals(other.gameObject.name))
                {
                    EnemySpawnFreq = item.EnemySpawnFreq;
                    StarterZombies = item.EnemyStartNumber;
                    activeZombieTypeZone = item.EnemyZombieTypesOnThatZone;
                }
            }

            for (int i = 0; i < StarterZombies; i++)
            {
                print(CheckWhichZoneName(player.transform.position));
                SpawningZombiesSolo(other.gameObject.name);
            }
            zone3 = true;
        }
        if (other.gameObject.name == "ZoneLvL4" && !zone4)
        {
            print("HE COLISIONADO CON Y SOY EL ENEMYASPAWNER" + other.gameObject.name);
            foreach (EnemyZones item in EnemyZoneLvls)
            {
                if (item.EnemyZoneName.Equals(other.gameObject.name))
                {
                    EnemySpawnFreq = item.EnemySpawnFreq;
                    StarterZombies = item.EnemyStartNumber;
                    activeZombieTypeZone = item.EnemyZombieTypesOnThatZone;
                }
            }

            for (int i = 0; i < StarterZombies; i++)
            {
                print(CheckWhichZoneName(player.transform.position));
                SpawningZombiesSolo(other.gameObject.name);
            }
            zone4 = true;
        }
        if (other.gameObject.name == "ZoneLvL5" && !zone5)
        {
            print("HE COLISIONADO CON Y SOY EL ENEMYASPAWNER" + other.gameObject.name);
            foreach (EnemyZones item in EnemyZoneLvls)
            {
                if (item.EnemyZoneName.Equals(other.gameObject.name))
                {
                    EnemySpawnFreq = item.EnemySpawnFreq;
                    StarterZombies = item.EnemyStartNumber;
                    activeZombieTypeZone = item.EnemyZombieTypesOnThatZone;
                }
            }

            for (int i = 0; i < StarterZombies; i++)
            {
                print(CheckWhichZoneName(player.transform.position));
                SpawningZombiesSolo(other.gameObject.name);
            }
            zone5 = true;
        }
        if (other.gameObject.name == "ZoneLvL2B" && !zone2B)
        {
            print("HE COLISIONADO CON Y SOY EL ENEMYASPAWNER" + other.gameObject.name);
            foreach (EnemyZones item in EnemyZoneLvls)
            {
                if (item.EnemyZoneName.Equals(other.gameObject.name))
                {
                    EnemySpawnFreq = item.EnemySpawnFreq;
                    StarterZombies = item.EnemyStartNumber;
                    activeZombieTypeZone = item.EnemyZombieTypesOnThatZone;
                }
            }

            for (int i = 0; i < StarterZombies; i++)
            {
                print(CheckWhichZoneName(player.transform.position));
                SpawningZombiesSolo(other.gameObject.name);
            }
            zone2B = true;
        }
        if (other.gameObject.name == "ZoneLvL4B" && !zone4B)
        {
            print("HE COLISIONADO CON Y SOY EL ENEMYASPAWNER" + other.gameObject.name);
            foreach (EnemyZones item in EnemyZoneLvls)
            {
                if (item.EnemyZoneName.Equals(other.gameObject.name))
                {
                    EnemySpawnFreq = item.EnemySpawnFreq;
                    StarterZombies = item.EnemyStartNumber;
                    activeZombieTypeZone = item.EnemyZombieTypesOnThatZone;
                }
            }

            for (int i = 0; i < StarterZombies; i++)
            {
                print(CheckWhichZoneName(player.transform.position));
                SpawningZombiesSolo(other.gameObject.name);
            }
            zone4B = true;
        }
        if (other.gameObject.name == "ZoneLvL5B" && !zone5B)
        {
            print("HE COLISIONADO CON Y SOY EL ENEMYASPAWNER" + other.gameObject.name);
            foreach (EnemyZones item in EnemyZoneLvls)
            {
                if (item.EnemyZoneName.Equals(other.gameObject.name))
                {
                    EnemySpawnFreq = item.EnemySpawnFreq;
                    StarterZombies = item.EnemyStartNumber;
                    activeZombieTypeZone = item.EnemyZombieTypesOnThatZone;
                }
            }

            for (int i = 0; i < StarterZombies; i++)
            {
                print(CheckWhichZoneName(player.transform.position));
                SpawningZombiesSolo(other.gameObject.name);
            }
            zone5B = true;
        }
    }
    public void playerZoneTransform()
    {
        foreach (SecureZone s in SecureZones)
        {
            if (s.zoneName == "PlayerZone")
            {
                s.zoneCenter = posPlayer;
            }
        }
    }
    public bool tryToActiveZone(string zoneName)
    {
        foreach (SecureZone s in SecureZones)
        {
            if (s.zoneName == zoneName)
            {
                if (!s.isActive)
                {
                    s.isActive = true;
                    return true;
                }
            }
        }
        return false;
    }

}
