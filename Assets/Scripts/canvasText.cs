﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class canvasText : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(Desactive());  
    }

    IEnumerator Desactive()
    {
        yield return new WaitForSeconds(10);
        this.gameObject.SetActive(false);
    }
}
