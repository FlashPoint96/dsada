﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FoodBarScript : MonoBehaviour
{

    public FPSPlayerStats PlayerStats;
    private float maxHunger;
    private float actualHunger;
    public TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
        maxHunger = PlayerStats.MaxFood;

    }
    public void UpdateHungry()
    {
        text.text = (int)PlayerStats.ActualFood + "/" + (int)PlayerStats.MaxFood;
        maxHunger = PlayerStats.MaxFood;
        actualHunger = PlayerStats.ActualFood;

        this.GetComponent<Image>().fillAmount = actualHunger / maxHunger;
    }
}
