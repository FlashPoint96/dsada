﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyBarController : MonoBehaviour
{
    public InteractionController enemy;
    public Text EnemyName;
    public GameObject EnemyBar;
    public Text ActualHpMSG;
    public GameEvent EnemyTargeted;
    

    // Start is called before the first frame update
    void Start()
    {
        EnemyBar.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        if (enemy.getEnemyTarget() != null)
        {
            EnemyTargeted.Raise();
            EnemyBar.SetActive(true);
            EnemyName.text = enemy.getEnemyTarget().name;
            ActualHpMSG.text = enemy.getEnemyTarget().actualHealth.ToString();
            if (enemy.getEnemyTarget().actualHealth <= 0)
            {
                print("ALOOOOO NO TENGO VIDAAAAA");
                EnemyBar.SetActive(false);

            }
        }
        else
        {
            EnemyBar.SetActive(false);
        }
      
   
    }
}
