﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarcadorArmasHUD : MonoBehaviour
{
    [SerializeField]
    private Image imgArma;

    [SerializeField]
    private Image imgBala;

    [SerializeField]
    private Text balas;
    public WeaponScriptableObject wso;
    public GameObject FPS;
    public FPSSwitchWeapon player;


    // Start is called before the first frame update
    void Start()
    {
        imgArma = this.transform.GetChild(0).GetComponent<Image>();
        imgBala = this.transform.GetChild(1).GetComponent<Image>();
        balas = this.transform.GetChild(2).GetComponent<Text>();

        FPS = GameObject.Find("FPSController");
        wso = FPS.GetComponent<FPSPlayerWeaponController>().GetFirstActiveChild().GetComponent<getWeaponSCO>().getWeapon();
  
    }

    // Update is called once per frame
    void Update()
    {
        load();
    }

    private void load()
    {
        wso = FPS.GetComponent<FPSPlayerWeaponController>().GetFirstActiveChild().GetComponent<getWeaponSCO>().getWeapon();

        if (wso.isRanged)
        {
            if(wso.weaponID == 101)
            {
                imgArma.enabled = false;
                imgBala.enabled = true;
                imgArma.sprite = wso.gunIcon;
                imgBala.sprite = wso.bulletIcon;
                balas.text = wso.maxShotsTotal + "/" + wso.actualShotsInCharger;

            }
            else
            {
                imgArma.enabled = true;
                imgBala.enabled = true;
                imgArma.sprite = wso.gunIcon;
                imgBala.sprite = wso.bulletIcon;
                balas.text = wso.maxShotsTotal + "/" + wso.actualShotsInCharger;
    
            }
        }
        else
        {
            imgArma.enabled = false;
            imgBala.enabled = false;
            balas.text = "";
        }
    }
}
