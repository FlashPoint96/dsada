﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StaminaBarScript : MonoBehaviour
{
    public FPSPlayerStats PlayerStats;
    private float MaxStamina;
    private float ActualStamina;
    public TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {

        MaxStamina = PlayerStats.MaxEnergy;
    }

    public void UpdateStamina()
    {
        text.text = (int)PlayerStats.ActualEnergy + "/" + (int)PlayerStats.MaxEnergy;
        MaxStamina = PlayerStats.MaxEnergy;
        ActualStamina = PlayerStats.ActualEnergy;

        this.GetComponent<Image>().fillAmount = ActualStamina / MaxStamina;


    }
}
