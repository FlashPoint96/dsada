﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthBarScript : MonoBehaviour
{

    public FPSPlayerStats PlayerStats;
    private float MaxHP;
    private float ActualHP;
    public TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
        
        MaxHP = PlayerStats.MaxHealth;
        
    }

    public void UpdateHealth()
    {
        text.text = (int)PlayerStats.ActualHealth + "/" + (int)PlayerStats.MaxHealth;
        MaxHP = PlayerStats.MaxHealth;
        ActualHP = PlayerStats.ActualHealth;
        
        this.GetComponent<Image>().fillAmount = ActualHP / MaxHP;


    }
        
}
