﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ThristBarScript : MonoBehaviour
{
    public FPSPlayerStats PlayerStats;
    private float MaxWater;
    private float ActualWater;
    public TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
        MaxWater = PlayerStats.MaxWater;
    }
    public void UpdateBar()
    {
        text.text = (int)PlayerStats.ActualWater + "/" + (int)PlayerStats.MaxWater;
        MaxWater = PlayerStats.MaxWater;
        ActualWater = PlayerStats.ActualWater;

        this.GetComponent<Image>().fillAmount = ActualWater / MaxWater;


    }

}
