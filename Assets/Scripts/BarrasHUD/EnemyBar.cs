﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyBar : MonoBehaviour
{
    public EnemyBarController EnemyStats;
    private float maxHP;
    private float actualHP;

    // Start is called before the first frame update
    void Start()
    {
        if (EnemyStats.enemy.getEnemyTarget() != null)
        {
            maxHP = EnemyStats.enemy.getEnemyTarget().maxHealth;
        }

    }

    public void UpdateEnemyBar()
    {
        if (EnemyStats.enemy.getEnemyTarget() != null)
        {
            maxHP = EnemyStats.enemy.getEnemyTarget().maxHealth;
            actualHP = EnemyStats.enemy.getEnemyTarget().actualHealth;

            this.GetComponent<Image>().fillAmount = actualHP / maxHP;



        }
    }

}
