﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonOnClickHotBar : MonoBehaviour
{
    private Item gun;
    private int SlotIndex;
    public InventoryItem inventario;
    private GameObject slotPrefab;
    private GameObject pj;
    private FPSPlayerStatsAsigner pj2;
    public Sprite fondo;
    //falta el gameobject pa ponerlo todo null del slot
    // Start is called before the first frame update
    void Start()
    {
        pj = GameObject.Find("FirstPersonCharacter");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setHotbarSlot(Item gun, int indexSlot,GameObject slotPrefab,FPSPlayerStatsAsigner pj)
    {
        this.SlotIndex = indexSlot;
        this.gun = gun;
        this.slotPrefab = slotPrefab;
        this.pj2 = pj;
    }
    public void onClickDoThis()
    {
        Object.Destroy(pj.transform.GetChild(SlotIndex).gameObject);
        pj.transform.GetChild(0).gameObject.SetActive(true);
        pj2.fps.GameObjectsweapons.Remove(pj2.fps.GameObjectsweapons[SlotIndex - 1]);
        slotPrefab.transform.GetChild(SlotIndex).GetChild(1).GetComponent<Image>().sprite = fondo;
        slotPrefab.transform.GetChild(SlotIndex).GetChild(2).GetComponent<Text>().text = "";
        slotPrefab.transform.GetChild(SlotIndex).GetChild(3).GetComponent<Text>().text= "";
        
        inventario.AddItem(new ItemObject(gun), 1);
        pj2.loadHotBar();
        //pj2.loadHotBar();
    }
    public void getHotbarSlot()
    {
    }
}
